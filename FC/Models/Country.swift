//
//  Country.swift
//  FC
//
//  Created by Fausto Savatteri on 28/09/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import ObjectMapper

class Country: Mappable {
    
    var countryId: Int = 0
    var countryName: String? = ""
    
    
    required convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    
    func mapping(map: Map) {
        countryId                        <- map["id"]
        countryName                    <- map["name"]
    }

}
