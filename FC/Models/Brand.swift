//
//  Brand.swift
//  FC
//
//  Created by Fausto Savatteri on 25/09/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import ObjectMapper

class Brand: Mappable {
    
    var brandId: Int = 0
    var brandName: String? = ""
    
    
    required convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    
    func mapping(map: Map) {
        brandId                        <- map["id"]
        brandName                    <- map["name"]
    }

}
