//
//  GalleryCall.swift
//  FC
//
//  Created by Fausto Savatteri on 05/11/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import UIKit
import SwiftKeychainWrapper


extension GalleryViewController {
    
    func getPostAddLastCall(success:@escaping ()->(), failure:@escaping (String)->()) {
           getPostAddLast(success: { responseData in
           do {
                         let jsonResult = try JSONSerialization.jsonObject(with: responseData, options:
                                                JSONSerialization.ReadingOptions.mutableContainers)
                                            FCSDK.postArray = []
                                            FCSDK.dotsArray = []
                                         

                                            if let jsonResultDictionary = jsonResult as? [String : Any],
                                                let data = jsonResultDictionary["data"] as? [[String : Any]] {
                                                for post in data {
                                                    if let mappedPost = Mapper<Post>().map(JSON: post) {
                                                    FCSDK.postArray.append(mappedPost)
                                                        
                                            
                                                    }
                                                    
                                                    if let mappedDots = Mapper<Dot>().map(JSON: post) {
                                                    FCSDK.dotsArray.append(mappedDots)
                                                        
                                            
                                                    }
                                                    
                                                }
                                                
                                                
                                              
                                             
                                                success()
                                            } else {
                                                failure(self.handleFailure(json: nil))
                                            }
                                        } catch {
                                            failure(self.handleFailure(json: nil))
                                        }
                            }, failure: { responseData in
                                failure(self.handleAlamofireError(responseData: responseData))
                            })
      }
    
    func getPostAddLast(success:@escaping (Data)->(), failure:@escaping (Any?)->()) {
        
        AF.request(FCSDK.rootUrl + "manage-posts/posts", method: .get, encoding: JSONEncoding.default).validate(statusCode: 200..<300).responseData { response in
                    //Utils.hideLoading()
                    
                    switch response.result {
                    case .success:
                        //if Utils.pickRandomBool(0.9){
                        if response.error == nil {
                            success(response.data!)
                        } else {
                            failure(response.data!)
                        }
                    case .failure(let error):
                        failure(error)
                    }
            }
    }
    
    
    func handleFailure(json: Any?) -> String {
        if let json = json as? [String: Any], let status = json["status"] as? String, status == "ko" {
            if let msgArray = json["msg"] as? NSArray {
                if let msg = msgArray[0] as? String {
                    return msg
                }
            }
        }
        return "error"
    }
    
    func handleAlamofireError(responseData: Any?) -> String {
        if let responseData = responseData as? Data {
            do {
                let jsonResult = try JSONSerialization.jsonObject(with: responseData, options:
                    JSONSerialization.ReadingOptions.mutableContainers)
                return self.handleFailure(json: jsonResult)
            } catch {
                return self.handleFailure(json: nil)
            }
        } else {
            return self.handleFailure(json: nil)
        }
    }
    
}
