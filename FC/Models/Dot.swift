//
//  Dot.swift
//  FC
//
//  Created by Fausto Savatteri on 29/09/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//


import Foundation
import ObjectMapper

class Dot: Mappable {
    
    var id: Int?
    var postId: Int?
    var posizioneX: Double? = 0.0
    var posizioneY: Double? = 0.0
    var description: String? = ""
    var itemId: Int?
    var colorId: Int?
    var brandId: Int?
    var gender: String? = ""
    var url: String? = ""

    
    required convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    
    func mapping(map: Map) {
        id                        <- map["id"]
        postId                        <- map["postid"]
        posizioneX                   <- map["dot_pos_x_perc"]
        posizioneY                   <- map["dot_pos_y_perc"]
        description                   <- map["description"]
        itemId                   <- map["itemid"]
        colorId                   <- map["colorid"]
        brandId                   <- map["brandid"]
        gender                   <- map["gender"]
        url                   <- map["url"]

    }
    
    func getDictFormat() -> [String: Any] {
        
        return ["id" : id ?? 1, "postId" : postId ?? 1, "dot_pos_x_perc" : posizioneX!, "dot_pos_y_perc" : posizioneY!,"description" : description ?? "ddd","itemid" : itemId ?? 1, "colorid" : colorId ?? 1, "brandid": brandId ?? 1, "gender": gender ?? "f", "url": url ?? "www.armani.com"]
    }
}
