//
//  ProfileCollectionViewCell.swift
//  FC
//
//  Created by Fausto Savatteri on 02/12/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import UIKit

class ProfileCollectionViewCell: UICollectionViewCell {
        
    
    @IBOutlet weak var profilePhoto: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   
}
