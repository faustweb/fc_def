//
//  TableViewSwipeCustomCell.swift
//  FC
//
//  Created by Fausto Savatteri on 29/09/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import UIKit
import FittedSheets
import BonMot
import Alamofire
import ObjectMapper


class TableViewSwipeCustomCell: UITableViewCell {
    
    @IBOutlet weak var photo: UIImageView!

    @IBOutlet var labelTitleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelTitle: UILabel!
    
    weak var viewController: FirstViewController?

    var dotIdSelezionato: Int?
    var contatoreDot = 0

    
    // ToDo Genere
    func setCell(photo: String){
        let _: UIImage = UIImage(named: photo)!
    }
    
    func setDot(dotArray: [Dot], view: UIView) {
        //MARK: clear per dots
        view.clearSubviews()
        contatoreDot = 0
        for dot in dotArray {
            let image = UIImage(named: "dot") as UIImage?
            let button   = UIButton(type: UIButton.ButtonType.custom) as UIButton
            
            if let posizioneX = dot.posizioneX,
                let posizioneY = dot.posizioneY {
                let larghezzaSchermo = viewController?.tableView.frame.size.width ?? 0.0
                let altezzaSchermo = viewController?.tableView.frame.size.height ?? 0.0
                
                let posX = CGFloat(posizioneX)
                let posXreale = posX * larghezzaSchermo
                let posY = CGFloat(posizioneY)
                let posYreale = posY * altezzaSchermo

                FCSDK.dotsIdForDetails?.append(dot.id!)
                dotIdSelezionato = dot.id

                FCSDK.nomeDotSelezionato = dot.description
                FCSDK.urlDotSelezionato = dot.url
                
                button.frame = CGRect(x: posXreale, y: posYreale, width: 50, height: 50)
                button.setImage(image, for: .normal)
                contatoreDot += 1
                button.tag = contatoreDot
                button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
                
                view.addSubview(button)
            }
             
        }
    }
    
    @objc func buttonAction(sender: UIButton!) {
        print("il tag del bottone è:")
        print(sender.tag)
        print("l'array dots ha dimensione:")
        print(FCSDK.dotsArrayInCellView.count)
        
        FCSDK.dotSelezionato = FCSDK.dotsArrayInCellView[sender.tag-1]
        //MARK: per visualizzare con popup dal basso
        let controller = SheetViewController(controller: UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailsDotVC"))
        viewController?.present(controller, animated: true, completion: nil)

    }
    
    func saveDotsArrayIEsimo() {
        
    }

    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }


}
