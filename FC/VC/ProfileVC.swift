//
//  ProfileVC.swift
//  FC
//
//  Created by Fausto Savatteri on 06/10/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//


import Foundation
import UIKit
import FittedSheets
import BonMot
import Alamofire


class ProfileVC: UIViewController {

    @IBOutlet weak var image1: UIImageView!
    
    var count = 0
    var userId: Int?
    
    @IBOutlet weak var labelProfileUserName: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!

    @IBAction func bellButton(_ sender: Any) {
        
        /*let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "goToFirstVC") // as! UIViewController
        secondViewController.modalPresentationStyle = .fullScreen
        self.present(secondViewController, animated: true, completion: nil)*/
    }

    @IBAction func backButton(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "goToFirstVC") // as! UIViewController
        secondViewController.modalPresentationStyle = .fullScreen
        self.present(secondViewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        searchOutlet.alpha = 0
        profileOutlet.alpha = 0
        cameraOutlet.alpha = 0
        settingsOutlet.alpha = 0
        
        image1.makeRounded()
        setPhoto(photo: image1)

        
         self.labelProfileUserName.text = FCSDK.userData?.nickname
        //print(FCSDK.userData?.firstname)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: (screenWidth / 3) - 8, height: screenWidth / 3)
            
        collectionView.collectionViewLayout = layout
    }
    
        @IBOutlet weak var fcButton: UIButton!
        @IBAction func FCButton(_ sender: Any) {
          count = count + 1
            
            
            if count.isEven {
                searchOutlet.alpha = 0
                profileOutlet.alpha = 0
                cameraOutlet.alpha = 0
                settingsOutlet.alpha = 0
            }
            else if count.isOdd {
                searchOutlet.alpha = 1
                profileOutlet.alpha = 1
                cameraOutlet.alpha = 1
                settingsOutlet.alpha = 1
                
                bubbleButton(button: searchOutlet)
                bubbleButton(button: profileOutlet)
                bubbleButton(button: cameraOutlet)
                bubbleButton(button: settingsOutlet)

            }

        }
        

        
        
        @IBOutlet weak var searchOutlet: UIButton!
        @IBOutlet weak var cameraOutlet: UIButton!
        @IBOutlet weak var settingsOutlet: UIButton!
        @IBOutlet weak var profileOutlet: UIButton!
        @IBAction func searchButton(_ sender: Any) {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "search") // as! UIViewController
            secondViewController.modalPresentationStyle = .fullScreen
            self.present(secondViewController, animated: true, completion: nil)
        }
        
        
        // FOTO
        @IBAction func cameraButton(_ sender: Any) {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "customCameraVC") // as! UIViewController
            secondViewController.modalPresentationStyle = .fullScreen
            self.present(secondViewController, animated: true, completion: nil)
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            picker.dismiss(animated: true)

            guard let image = info[.editedImage] as? UIImage else {
                print("No image found")
                return
            }

            // print out the image size as a test
            print(image.size)
        }
        
        // SOS BUTTON
        @IBAction func settingsButton(_ sender: Any) {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "sos")
            secondViewController.modalPresentationStyle = .fullScreen
            self.present(secondViewController, animated: true, completion: nil)

            
        }
        @IBAction func profileButton(_ sender: Any) {
        }
        

        
        func bubbleButton(button: UIButton) {
            
            button.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)

            UIView.animate(withDuration: 1.0,
              delay: 0,
              usingSpringWithDamping: 0.2,
              initialSpringVelocity: 1.8,
              options: .allowUserInteraction,
              animations: { [weak button] in
                button?.transform = .identity
              },
              completion: nil)
            
        }
        
    func setPhoto(photo: UIImageView) {
        
        for element in FCSDK.postArray {
            if element.userId == FCSDK.currentUserId {
                let imageUrl = element.urlThumbImg
                let url = URL(string: imageUrl)
                photo.kf.setImage(with: url)
            }
        }
    }
        
        
}
    

