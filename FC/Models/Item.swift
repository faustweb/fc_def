//
//  Item.swift
//  FC
//
//  Created by Fausto Savatteri on 25/09/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import ObjectMapper

class Item: Mappable {
    
    var itemId: Int = 0
    var itemName: String? = ""
    
    
    required convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    
    func mapping(map: Map) {
        itemId                        <- map["id"]
        itemName                    <- map["name"]
    }

}
