//
//  FCSDK.swift
//  FC
//
//  Created by Fausto Savatteri on 30/06/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import UIKit
import Swift
import Alamofire
import ObjectMapper
import DropDown


class BrandItem : NSObject {
    
    var id: String?
    var category : String?  //pantalone, gonna, giacca, ecc.
    var brandName : String?
    var coloreAssegnato : UIColor?
    var isPreferito = false
    var isSelected = false
    var imageUrl : String?
    
    override init() {
    }
}

class FCRegistrationData{
    var email:String?
    var password: String?
    var password_confirmation: String?
    var terms_of_service: Bool?
    var locale: String?
    var first_name: String?
    var last_name: String?
    var username: String?
    //--
    var trusted_token: String?
    var trusted_secret: String?
    var uid: String?
    var provider: String?
    var client_secret: String? // = "0792cece85b36432ede507688855f249ffcbcad30f3475291787432a47fdd648"
    var client_id: String? // = "c8b92887e7d2866495460f940886b882b3b09da0b0832a3c5329a86b513c4cbd"
    var grant_type: String? // = "password"
    var accessToken: String?
}



class FCSDK {
    //old dns "http://ec2-15-236-218-151.eu-west-3.compute.amazonaws.com/api/fc/auth/"
    static var rootUrl = "http://ec2-35-180-44-84.eu-west-3.compute.amazonaws.com/api/fc/"
    //Alamofire
    
    
    static var userArray :[User] = []
    static var userDataArray :[UserData] = []
    static var userData: UserData?
    static var currentUserId : Int = 0

    static var dataArray: [DataSDK] = []
    
    static var itemsArray :[Item] = []
    static var colorArray: [Color] = []
    static var circleArray: [Circle] = []
    static var brandsArray :[Brand] = []
    static var countryArray :[Country] = []
    
    
    static var postArray :[Post] = []
    static var dotsArray :[Dot] = []
    static var dotDictionary: [String:Any]?
    static var dotsDictionaryArray: [[String:Any]] = [[:]]
    static var newDotForGalleryPost: Dot?

    
    static var errorNewUser: Bool = false
    static var newUserResult : String?
    static var errorCode : Int? = 0
    
    static var filterIsSelected: Bool = false
    static var filterLabelSelection: String = "ALL"
    static var filterLabel2: String = ""
    static var filterLabel3: String = ""
    static var filterLabel4: String = ""
    static var filterNumbers = 0

    
    
    static var rigaDaColorare: IndexPath?
    static var sezioneDaColorare: Int = 0

    static var daColorareAIndex: IndexPath?


    static var dotsArrayInCellView: [Dot] = []
    static var dotSelezionato: Dot?


    
    static var photoIdToVote = 1
    static var itemIdToSend = 0
    static var brandIdToSend = 0
    static var circleIdToSend = 0
    static var countryIdToSend = 0
    static var colorIdToSend = 0
    
    static var parametersQueryFiltered = ""
    static var stringToSendForFilter = ""
    
    static var neroInserito = false
    static var grigioInserito = false
    static var violaInsrito = false
    static var bluInserito = false
    static var azzurroInserito = false
    static var verdeInserito = false
    static var rossoInsrito = false
    static var arancioneInsrito = false
    static var biancoInserito = false

    static var addNewPhotoToArray = false
    
    static var pickFromGallery = false
    static var pickFromCamera = false
    static var photoTaken: UIImage?
    
    static var aggiungiDot = false

    static var chooseDropDown = DropDown()
    
    static var menu: DropDown = {
        let menu = DropDown()
        menu.dataSource = ["Rome", "London", "Milan", "NYC", "Paris"]
        
        //menu.dataSource = FCSDK.countryArray[index].countryName
        
        menu.cellNib = UINib(nibName: "DropDownCell", bundle: nil)
        menu.customCellConfiguration = {index, title, cell in
            guard let cell = cell as? MyCell else {
                return
            }
            //cell.myImageView.image = UIImage(systemName: "bookmark")
            //cell.optionLabel.text = FCSDK.countryArray[index].countryName
        }
        return menu
    }()

    static var nomeDotSelezionato: String?
    static var urlDotSelezionato: String?
    static var dotsIdForDetails: [Int]?
    static var dotIdSelezionato: Int?
    
    static func coloriResetToTrue() {
        neroInserito = true
        grigioInserito = true
        violaInsrito = true
        bluInserito = true
        azzurroInserito = true
        verdeInserito = true
        rossoInsrito = true
        arancioneInsrito = true
        biancoInserito = true
    }
    
    static func daColorareAtIndexAndSection(index: IndexPath, section: Int) {
        FCSDK.rigaDaColorare = index
        FCSDK.sezioneDaColorare = section
    }

    
    static func testPost () {
        
        let urlTest = URL(string: "https://jsonplaceholder.typicode.com/todos")
                                                guard let requestUrl = urlTest else { fatalError() }
                                                // Prepare URL Request Object
                                                var request = URLRequest(url: requestUrl)
                                                request.httpMethod = "POST"
                                                print("Ho invocato il metodo post")
                    
                                                // HTTP Request Parameters which will be sent in HTTP Request Body
                                                let postString = "userId=300&title=My urgent task&completed=false";
                                                // Set HTTP Request Body
                                                request.httpBody = postString.data(using: String.Encoding.utf8);
                                                // Perform HTTP Request
                                                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                                                        
                                                        // Check for Error
                                                        if let error = error {
                                                            print("Error took place \(error)")
                                                            return
                                                        }
                                                 
                                                        // Convert HTTP Response Data to a String
                                                        if let data = data, let dataString = String(data: data, encoding: .utf8) {
                                                            print("Response data string:\n \(dataString)")
                                                        }
                                                }
                                                task.resume()
    }
    
    
    
    static func getTest () {
        // Create URL
        let url = URL(string: "https://jsonplaceholder.typicode.com/todos/1")
        guard let requestUrl = url else { fatalError() }
        // Create URL Request
        var request = URLRequest(url: requestUrl)
        // Specify HTTP Method to use
        request.httpMethod = "GET"
        // Send HTTP Request
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            // Check if Error took place
            if let error = error {
                print("Error took place \(error)")
                return
            }
            
            // Read HTTP Response Status code
            if let response = response as? HTTPURLResponse {
                print("Response HTTP Status code: \(response.statusCode)")
            }
            
            // Convert HTTP Response Data to a simple String
            if let data = data, let dataString = String(data: data, encoding: .utf8) {
                print("Response data string:\n \(dataString)")
            }
            
        }
        task.resume()
    }
    
    
    static func postLogin () {
        let urlRoot = URL(string: "\(FCSDK.rootUrl)auth/login")
                                                guard let requestUrl = urlRoot else { fatalError() }
                                                // Prepare URL Request Object
                                                var request = URLRequest(url: requestUrl)
                                                request.httpMethod = "POST"
                                                print("Ho invocato il metodo post per Login")
                    
                                                // HTTP Request Parameters which will be sent in HTTP Request Body
                                                let postString = "username=testuser2";
                                                // Set HTTP Request Body
                                                request.httpBody = postString.data(using: String.Encoding.utf8);
                                                // Perform HTTP Request
                                                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                                                        
                                                        // Check for Error
                                                        if let error = error {
                                                            print("Error took place \(error)")
                                                            return
                                                        }
                                                 
                                                        // Convert HTTP Response Data to a String
                                                        if let data = data, let dataString = String(data: data, encoding: .utf8) {
                                                            print("Response data string:\n \(dataString)")
                                                        }
                                                }
                                                task.resume()
    }
    
    
    static func getLoggedUser () {
        let url = URL(string: "\(FCSDK.rootUrl)auth/loggeduser")
                                            // Create URL
                                            guard let requestUrl = url else { fatalError() }
                                            // Create URL Request
                                            var request = URLRequest(url: requestUrl)
                                            // Specify HTTP Method to use
                                            request.httpMethod = "GET"
                                            // Send HTTP Request
                                            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                                                
                                                // Check if Error took place
                                                if let error = error {
                                                    print("Error took place \(error)")
                                                    return
                                                }
                                                
                                                // Read HTTP Response Status code
                                                if let response = response as? HTTPURLResponse {
                                                    print("Response HTTP Status code per Logged User: \(response.statusCode)")
                                                }
                                                
                                                // Convert HTTP Response Data to a simple String
                                                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                                                    print("Response data string:\n \(dataString)")
                                                }
                                                
                                            }
                                            task.resume()
    }
    
    
    

    

    // funzione per la generazione del json a partire da un Data
    static func json_parseData(_ data: Data) -> [String:Any]? {
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            print("[JSON] OK!")
            print(json)
            return (json as? [String:Any])
        } catch _ {
            print("[ERROR] An error has happened with parsing of json data")
            return nil
        }
        
    }


    // funzione per il download della pagina dato un URL e conseguente conversione del download in Data
     // la funzione accetta come parametro in ingresso una String per identificare il comune
     // e ritorna un Data se il download è avvenuto o nil se c'è stato un problema
     static func fcData_request(forType: String) -> Data? {
         
         guard let url = URL(string: "\(FCSDK.rootUrl)environment/data") else {
             return nil
         }
         
         guard let data = try? Data(contentsOf: url) else {
             print("[ERROR] There is an unspecified error with the connection")
             return nil
         }
         
         print("[CONNECTION] OK, data correctly downloaded")
         return data
     }
    
    
    
     /*static func createCellsContent(collectionView: UICollectionView, colors: [Color]?, item: [Item]?) -> [[String : Any]] {
         if let colors = colors {
             var cellsInfoColorsArray = [[String : Any]]()

             for color in colors {
                 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: IndexPath.init(row: 0, section: 0)) as! ItemsCollectionViewCell
                
                    //cell.brandLabel.text = color.colorName
                cell.brandLabel.text = "SUCA"

                    var textDictionary = [String : Any]()

                //textDictionary["id"] = color.colorId
                //textDictionary["name"] = color.colorName

                    textDictionary["rgb"] = color.colorRGB
               
                    cellsInfoColorsArray.append(textDictionary)
             }
             return cellsInfoColorsArray
         }
          return [[:]]
    }*/
    
    
    
       static func getPostCall(success:@escaping ()->(), failure:@escaping (String)->()) {
             getPost(success: { responseData in
              do {
                            let jsonResult = try JSONSerialization.jsonObject(with: responseData, options:
                                                   JSONSerialization.ReadingOptions.mutableContainers)
                                               FCSDK.postArray = []
                                               FCSDK.dotsArray = []
                                            

                                               if let jsonResultDictionary = jsonResult as? [String : Any],
                                                   let data = jsonResultDictionary["data"] as? [[String : Any]] {
                                                   for post in data {
                                                       if let mappedPost = Mapper<Post>().map(JSON: post) {
                                                       FCSDK.postArray.append(mappedPost)
                                                           
                                                           //FCSDK.dotsArray.append(mappedPost.dots)
                                               
                                                       }
                                                   }
                                                                                                   
                                                   success()
                                               } else {
                                                   failure(self.handleFailure(json: nil))
                                               }
                                           } catch {
                                               failure(self.handleFailure(json: nil))
                                           }
                               }, failure: { responseData in
                                   failure(self.handleAlamofireError(responseData: responseData))
                               })
         }
       
       static func getPost(success:@escaping (Data)->(), failure:@escaping (Any?)->()) {
           
           AF.request(FCSDK.rootUrl + "manage-posts/posts", method: .get, encoding: JSONEncoding.default).validate(statusCode: 200..<300).responseData { response in
                       //Utils.hideLoading()
                       
                       switch response.result {
                       case .success:
                           //if Utils.pickRandomBool(0.9){
                           if response.error == nil {
                               success(response.data!)
                           } else {
                               failure(response.data!)
                           }
                       case .failure(let error):
                           failure(error)
                       }
               }
       }
       
       
       static func handleFailure(json: Any?) -> String {
           if let json = json as? [String: Any], let status = json["status"] as? String, status == "ko" {
               if let msgArray = json["msg"] as? NSArray {
                   if let msg = msgArray[0] as? String {
                       return msg
                   }
               }
           }
           return "error"
       }
       
       static  func handleAlamofireError(responseData: Any?) -> String {
           if let responseData = responseData as? Data {
               do {
                   let jsonResult = try JSONSerialization.jsonObject(with: responseData, options:
                       JSONSerialization.ReadingOptions.mutableContainers)
                   return self.handleFailure(json: jsonResult)
               } catch {
                   return self.handleFailure(json: nil)
               }
           } else {
               return self.handleFailure(json: nil)
           }
       }
       

    
      static func postSendPostCall(caption: String, caption_pos_y_perc: Double, dots: [[String:Any]], image: String, success:@escaping ()->(), failure:@escaping (String)->()) {
           postSendPost(caption: caption, caption_pos_y_perc: caption_pos_y_perc, dots: dots, image: image, success: { responseData in
               print("success")
                               }, failure: { responseData in
                                   failure(FCSDK.handleAlamofireError(responseData: responseData))
                               })
         }
       
       static func postSendPost(caption: String, caption_pos_y_perc: Double, dots: [[String:Any]], image: String, success:@escaping (Data)->(), failure:@escaping (Any?)->()) {
           

          
           
           
           let parameters: Parameters = ["caption": caption, "caption_pos_y_perc": caption_pos_y_perc, "dots": dots, "image": image ]

            
           
           AF.request(FCSDK.rootUrl + "manage-posts/posts", method: .post, parameters: parameters, encoding: JSONEncoding.default).validate(statusCode: 200..<300).responseData { response in
            
            

               
                       switch response.result {
                       case .success:
                           if response.error == nil {
                               success(response.data!)
                               debugPrint("Response: \(response)")
                               
                           } else {
                               failure(response.data!)
                           }
                       case .failure(let error):
                           failure(error)
                       }
               }
           //} //end for ObjectMapper
       }
       
}
