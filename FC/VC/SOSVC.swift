//
//  SOSVC.swift
//  FC
//
//  Created by Fausto Savatteri on 06/10/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import UIKit
import FittedSheets
import BonMot
import Alamofire


class SOSVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var textSOS: UITextField!
    
    var count = 0
    var isSelected5 = false
    var isSelected30 = false
    var isSelected60 = false
    
    var uno = false
    var due = false
    
    var imagePicker = UIImagePickerController()
    var imagePicker2 = UIImagePickerController()
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageView2: UIImageView!
    
    @IBOutlet weak var upLoadImageBtn:UIImage!
    

    

    @IBOutlet weak var min60Outlet: UIButton!
    @IBOutlet weak var min30Outlet: UIButton!
    @IBOutlet weak var min5Outlet: UIButton!


    @IBAction func Button5mins(_ sender: Any) {
        
        min5Outlet.backgroundColor = UIColor.black
        isSelected5 = true
        min5Outlet.setTitleColor(.white, for: .normal)

        
        if isSelected30 == true {
            min30Outlet.backgroundColor = UIColor.white
            isSelected30 = false
            min30Outlet.setTitleColor(.black, for: .normal)

            
        }
        
        if isSelected60 == true {
            min60Outlet.backgroundColor = UIColor.white
            isSelected60 = false
            min60Outlet.setTitleColor(.black, for: .normal)

        }
        
    }
    
    @IBAction func Button30mins(_ sender: Any) {
        min30Outlet.backgroundColor = UIColor.black
        min30Outlet.setTitleColor(.white, for: .normal)

        isSelected30 = true
        
        if isSelected60 == true {
            min60Outlet.backgroundColor = UIColor.white
            isSelected60 = false
            min60Outlet.setTitleColor(.black, for: .normal)

        }
        
        if isSelected5 == true {
            min5Outlet.backgroundColor = UIColor.white
            isSelected5 = false
            min5Outlet.setTitleColor(.black, for: .normal)

        }
    }
    
    @IBAction func Button60mins(_ sender: Any) {
        
        min60Outlet.backgroundColor = UIColor.black
        isSelected60 = true
        min60Outlet.setTitleColor(.white, for: .normal)

        
        if isSelected30 == true {
            min30Outlet.backgroundColor = UIColor.white
            isSelected30 = false
            min30Outlet.setTitleColor(.black, for: .normal)

        }
        if isSelected5 == true {
            min5Outlet.backgroundColor = UIColor.white
            isSelected5 = false
            min5Outlet.setTitleColor(.black, for: .normal)

        }
    }
    
    @IBAction func ShareButton(_ sender: Any) {
        
        
            // create the alert
            let alert = UIAlertController(title: "", message: nil, preferredStyle: UIAlertController.Style.alert)
            
            //alert.setTitle(font: UIFont.boldSystemFont(ofSize: 26), color: UIColor.white)
            alert.setBackgroudColor(color: UIColor.black)

            let attributedString = NSAttributedString(string: "Il tuo SOS è stato inviato!", attributes: [
                NSAttributedString.Key.font : UIFont.systemFont(ofSize: 20), //your font here
                NSAttributedString.Key.foregroundColor : UIColor.white])
        
            alert.setValue(attributedString, forKey: "attributedTitle")

            let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
                print("TEST")
                //MARK: update, torno a home e non visualizzo share with
                let firstVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "goToFirstVC") as? FirstViewController
                firstVC?.modalPresentationStyle = .fullScreen
                self.present(firstVC!, animated: true, completion: nil)
            })
            okButton.setValue(UIColor.white, forKey: "titleTextColor")
            alert.addAction(okButton)

            // show the alert
            self.present(alert, animated: true, completion: nil)
            
        
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "goToFirstVC")
        secondViewController.modalPresentationStyle = .fullScreen
        self.present(secondViewController, animated: true, completion: nil)
    }

    @IBOutlet weak var pickerButtonOutlet: UIButton!
    @IBOutlet weak var picker2ButtonOutlet: UIButton!
    
    @IBAction func pickImageButton(_ sender: Any) {
        
           uno = true
           pickerButtonOutlet.isHidden = true
        
           imagePicker.allowsEditing = false
           imagePicker.sourceType = .photoLibrary

           present(imagePicker, animated: true, completion: nil)

    }
    
    @IBAction func pickImage2Button(_ sender: Any) {
        
           due = true
           picker2ButtonOutlet.isHidden = true
        
           imagePicker2.allowsEditing = false
           imagePicker2.sourceType = .photoLibrary

           present(imagePicker2, animated: true, completion: nil)

    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        

        min5Outlet.setTitle("5 mins", for: .normal)
        min30Outlet.setTitle("30 mins", for: .normal)
        min60Outlet.setTitle("60 mins", for: .normal)
        min5Outlet.setTitleColor(.black, for: .normal)
        min30Outlet.setTitleColor(.black, for: .normal)
        min60Outlet.setTitleColor(.black, for: .normal)

        min5Outlet.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        //min5Outlet.contentVerticalAlignment = UIControl.ContentVerticalAlignment.top
        min30Outlet.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        //min30Outlet.contentVerticalAlignment = UIControl.ContentVerticalAlignment.top
        min60Outlet.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        //min60Outlet.contentVerticalAlignment = UIControl.ContentVerticalAlignment.top
        
        min5Outlet.backgroundColor = UIColor.white
        min30Outlet.backgroundColor = UIColor.white
        min60Outlet.backgroundColor = UIColor.white


        imagePicker.delegate = self
        imagePicker2.delegate = self

        //MARK: Text field trasparente e delegate (hide keyboard on return button)
        textSOS.delegate = self
    }
      
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                    self.imageView.contentMode = .scaleAspectFit
                    
                if uno == true {
                    self.imageView.image = pickedImage
                }
                else if due == true {
                    self.imageView2.image = pickedImage
                }
                
                else if uno == true && due == false {
                    self.imageView.image = pickedImage
                }
                else if due == true && uno == false {
                    self.imageView2.image = pickedImage
                }
            }
     
        dismiss(animated: true, completion: nil)
    }
        
    // MARK: - Dismiss Keyboard Method
     func textFieldShouldReturn(_ textField: UITextField) -> Bool
     {

         textField.resignFirstResponder()
         return true
     }
        
}
    



extension UIAlertController {

  //Set background color of UIAlertController
  func setBackgroudColor(color: UIColor) {
    if let bgView = self.view.subviews.first,
      let groupView = bgView.subviews.first,
      let contentView = groupView.subviews.first {
      contentView.backgroundColor = color
    }
  }
}
