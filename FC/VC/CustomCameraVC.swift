//
//  CustomCameraVC.swift
//  FC
//
//  Created by Fausto Savatteri on 30/10/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import UIKit
import AVFoundation

class CustomCameraController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    @IBOutlet var cameraButton:UIButton!
    
    @IBOutlet var galleryButton:UIButton!

    @IBOutlet var imageViewForPhoto: UIImageView!
    
    var backFacingCamera: AVCaptureDevice?
    var frontFacingCamera: AVCaptureDevice?
    var currentDevice: AVCaptureDevice!
    
    var stillImageOutput: AVCapturePhotoOutput!
    var capturePhotoOutput: AVCapturePhotoOutput?

    var stillImage: UIImage?
    
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    
    //var captureSession = AVCaptureSession()
    var captureSession: AVCaptureSession?

    
    var imagePicker = UIImagePickerController()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
           // Setup your camera here...
        configure()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.captureSession.stopRunning()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action methods
    
    @IBAction func capture(sender: UIButton) {
        // Set photo settings
        let photoSettings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        photoSettings.isHighResolutionPhotoEnabled = true
        photoSettings.flashMode = .auto
        
        stillImageOutput.isHighResolutionCaptureEnabled = true
        stillImageOutput.capturePhoto(with: photoSettings, delegate: self)
        
        FCSDK.pickFromCamera = true

        
    }
    
    @IBAction func unwindToCameraView(sender: UIButton) {
        FCSDK.photoTaken = UIImage() //inizializzo la foto (nel caso in cui fosse stata scattata e scartata)
        
        FCSDK.pickFromGallery = true
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "cameraRoll") // as! UIViewController
        secondViewController.modalPresentationStyle = .fullScreen
        self.present(secondViewController, animated: true, completion: nil)
    }
    
    // MARK: - Helper methods
    
    private func configure() {
        // Preset the session for taking photo in full resolution
        //captureSession.sessionPreset = AVCaptureSession.Preset.photo
        
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video as the media type parameter
        guard let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {
            print("No video device found")
            return
        }
                
        //NEW DEBUG START
        guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
            else {
                print("Unable to access back camera!")
                return
        }
        
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous deivce object
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Initialize the captureSession object
            captureSession = AVCaptureSession()
            
            // Set the input devcie on the capture session
            captureSession?.addInput(input)

            // Get an instance of ACCapturePhotoOutput class
            capturePhotoOutput = AVCapturePhotoOutput()
            capturePhotoOutput?.isHighResolutionCaptureEnabled = true
            
            stillImageOutput = AVCapturePhotoOutput()
            stillImageOutput?.isHighResolutionCaptureEnabled = true

            
            // Set the output on the capture session
            //captureSession?.addOutput(capturePhotoOutput!)
            captureSession?.addOutput(stillImageOutput)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the input device
            /*let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)

            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]*/
            
            // Provide a camera preview
            cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
            view.layer.addSublayer(cameraPreviewLayer!)
            cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            cameraPreviewLayer?.frame = view.layer.frame
            
            // Bring the camera button to front
            view.bringSubviewToFront(cameraButton)
            view.bringSubviewToFront(galleryButton)
            captureSession?.startRunning()
            
            }
            catch {
            //If any error occurs, simply print it out
            print(error)
            return
        }
        
        /*do {
            let input = try AVCaptureDeviceInput(device: backCamera)
            //Step 9
            
            // Get the front and back-facing camera for taking photos
            let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera], mediaType: AVMediaType.video, position: .unspecified)
            
            for device in deviceDiscoverySession.devices { 
                if device.position == .back {
                    backFacingCamera = device
                } else if device.position == .front {
                    frontFacingCamera = device
                }
            }
            
            currentDevice = backFacingCamera
        }
        catch let error  {
            print("Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
        //END NEW DEBUG
        
       
        
        guard let captureDeviceInput = try? AVCaptureDeviceInput(device: currentDevice) else {
            FCSDK.photoTaken = UIImage() //inizializzo la foto (nel caso in cui fosse stata scattata e scartata)
            
            FCSDK.pickFromGallery = true
            return print("non c'è fotocamera disponibile come input")
        }
        
        // Configure the session with the output for capturing still images
        stillImageOutput = AVCapturePhotoOutput()
        
        // Configure the session with the input and the output devices
        captureSession.addInput(captureDeviceInput)
        captureSession.addOutput(stillImageOutput)
        
        // Provide a camera preview
        cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        view.layer.addSublayer(cameraPreviewLayer!)
        cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        cameraPreviewLayer?.frame = view.layer.frame
        
        // Bring the camera button to front
        view.bringSubviewToFront(cameraButton)
        view.bringSubviewToFront(galleryButton)
        captureSession.startRunning()*/
        
    }
}

extension CustomCameraController: AVCapturePhotoCaptureDelegate {
    /*func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        guard error == nil else {
            return
        }
        
        // Get the image from the photo buffer
        guard let imageData = photo.fileDataRepresentation() else {
            return
        }
        

        
        stillImage = UIImage(data: imageData)
    }*/
    
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        // capture image finished
        print("Image captured.")
        if let imageData = photo.fileDataRepresentation() {
            if let uiImage = UIImage(data: imageData){
                // do stuff to UIImage
                
                
                //self.imageViewForPhoto.contentMode = .scaleAspectFit
                //self.imageViewForPhoto.image = uiImage
                //view.bringSubviewToFront(imageViewForPhoto)
                FCSDK.photoTaken = uiImage
                

                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let secondViewController = storyboard.instantiateViewController(withIdentifier: "cameraRoll") // as! UIViewController
                secondViewController.modalPresentationStyle = .fullScreen
                self.present(secondViewController, animated: true, completion: nil)

            }
        }
    }
    
    /*func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
    // get captured image
        
        // Make sure we get some photo sample buffer
        guard error == nil,
        let photoSampleBuffer = photoSampleBuffer else {
        print("Error capturing photo: \(String(describing: error))")
        return
        }
        // Convert photo same buffer to a jpeg image data by using // AVCapturePhotoOutput
        guard let imageData =
        AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: photoSampleBuffer, previewPhotoSampleBuffer: previewPhotoSampleBuffer) else {
        return
        }
        
        
        
        // Initialise a UIImage with our image data
        let capturedImage = UIImage.init(data: imageData , scale: 1.0)
        if let image = capturedImage {
        // Save our captured image to photos album
        //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
            //self.imageViewForPhoto = image
            self.imageViewForPhoto.contentMode = .scaleAspectFit
            self.imageViewForPhoto.image = image
        }
    }*/
    
    
    enum ImageSource {
        case photoLibrary
        case camera
    }
    
    func selectImageFrom(_ source: ImageSource){
           imagePicker =  UIImagePickerController()
           imagePicker.delegate = self
           switch source {
           case .camera:
               imagePicker.sourceType = .camera
           case .photoLibrary:
               imagePicker.sourceType = .photoLibrary
           }
           present(imagePicker, animated: true, completion: nil)
       }
}
