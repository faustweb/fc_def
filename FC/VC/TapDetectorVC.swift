//
//  TapDetectorVC.swift
//  FC
//
//  Created by Fausto Savatteri on 07/10/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import UIKit
import FittedSheets
import BonMot
import Alamofire
import Kingfisher
import DropDown

extension CameraViewController {
    
    
    func doubleTapDetected() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTappedSetDotButton))
              tap.numberOfTapsRequired = 2
              view.addGestureRecognizer(tap)
    }
    
    @objc func doubleTappedSetDotButton() {
         print("posso aggiungere dot, fatti due tocchi, poi diventa LONG TOUCH")
         //if FCSDK.filterIsSelected == false {
         let image = UIImage(named: "dot") as UIImage?
         let button   = UIButton(type: UIButton.ButtonType.custom) as UIButton
         button.frame = CGRect(x: self.dotX, y: self.dotY, width: 50, height: 50)
         button.setImage(image, for: .normal)
         
         self.view.addSubview(button)
         self.imageView.addSubview(button)

     }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        if let touch = touches.first,
            self.tagIsSelected == true {
            print("ho toccato lo schermo: SINGLE TAP")
            let position = touch.location(in: self.view)
             print(position.x)
             print(position.y)
            
            self.dotX = position.x
            self.dotY = position.y
            
            print("creo il bottone DOT")
            let image = UIImage(named: "dot") as UIImage?
            let button   = UIButton(type: UIButton.ButtonType.custom) as UIButton
            button.frame = CGRect(x: self.dotX, y: self.dotY, width: 50, height: 50)
            button.setImage(image, for: .normal)
                    
            self.imageView.addSubview(button)
            
            //MARK: memorizzo il dot
            let newDot = Dot()
            newDot.posizioneX = Double(self.dotX)/1000
            newDot.posizioneY = Double(self.dotY)/1000
            newDot.description = "Descrizione Nuovo Articolo"
            newDot.brandId = 2
            newDot.colorId = 3
            newDot.itemId = 1
            newDot.gender = "f"
            
            
            //self.newPost.dots?.append(newDot)
            self.dots.append(newDot)
            print(self.dots.count)
            
            //MARK: creo form per inserire dettagli su articolo
            let dropDown = DropDown()
            setItemDotDetails(dropDown: dropDown)
            
         }
        
    }
    
    func setItemDotDetails(dropDown: DropDown) {
        
        // When drop down is displayed with `Direction.top`, it will be above the anchorView
        dropDown.topOffset = CGPoint(x: 0, y: (view.frame.size.width * view.frame.size.width)/2)
        
        dropDown.width = view.frame.size.width
        dropDown.cellHeight = 200

        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = ["Item", "Color", "Brand", "Gender"]
        
        /*** IMPORTANT PART FOR CUSTOM CELLS ***/
        dropDown.cellNib = UINib(nibName: "PopupLocationCell", bundle: nil)
        

        
        DropDown.appearance().backgroundColor = UIColor.black
        DropDown.appearance().textColor = UIColor.white
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
           guard let cell = cell as? PopupLocationCell else { return }

           // Setup your custom UI components
            cell.setupChooseDropDown()
            
            dropDown.selectionAction = { index, title in
                print("Indice Dot \(index) e \(title)")
            }
            
        }
        /*** END - IMPORTANT PART FOR CUSTOM CELLS ***/

        dropDown.show()
        dropDown.dismissMode = .onTap
    }
    
    
}
