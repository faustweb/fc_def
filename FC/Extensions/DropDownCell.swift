//
//  MyCell.swift
//  FC
//
//  Created by Fausto Savatteri on 12/10/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import UIKit
import DropDown

class MyCell: DropDownCell {
    
    @IBOutlet var myImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //self.myImageView.contentMode = .scaleAspectFit
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
