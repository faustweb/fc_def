//
//  LoginViewController.swift
//  FC
//
//  Created by Fausto Savatteri on 14/10/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//


import Foundation
import UIKit
import FittedSheets
import BonMot
import Alamofire


class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.delegate = self
    }
    
    
    @IBAction func loginButton(_ sender: Any) {
        
//        if textField.text == "" {
//
//            let alert = UIAlertController(title: "Enter a valid username", message: nil, preferredStyle: .alert)
//
//              alert.addAction(UIAlertAction(title: "RETRY", style: .default, handler: nil))
//
//              self.present(alert, animated: true)
//
//        } else {
        textField.text = "aaaa"
            login()
//        }
       
    }
    
    
    func login() {
        postLoginCall(username: textField.text!, nickname: textField.text!, success:{
            
            for element in FCSDK.userArray {
                FCSDK.userData = element.userdata
                FCSDK.currentUserId = element.userdata?.id ?? 0
            }
            
            self.getDataCall(success:{}, failure: { msg in
                print("error")
            })
            
            self.getPostCall(success:{
                                
                let firstVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "goToFirstVC") as? FirstViewController
                firstVC?.modalPresentationStyle = .fullScreen
                self.present(firstVC!, animated: true, completion: nil)
                               
            }, failure: { responseData in
                print(responseData)
            })
            
        }, failure: { msg in
            print("error, impossibile connettersi")
            let alert = UIAlertController(title: "Access Denied", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "RETRY", style: .default, handler: nil))
            self.present(alert, animated: true)
        })
    }
    
    
    // MARK: - Dismiss Keyboard Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        login()
        return true
    }
}
