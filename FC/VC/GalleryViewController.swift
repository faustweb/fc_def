//
//  CameraViewController.swift
//  FC
//
//  Created by Fausto Savatteri on 02/07/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import UIKit
import FittedSheets
import DropDown
import AVFoundation


class GalleryViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate, UICollectionViewDelegate, UITextFieldDelegate {
    
    let newPost = Post()
    var dots = [Dot]()
    var dotX: CGFloat = 0
    var dotY: CGFloat = 0
    let recognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressHappened))
    
    //let doubleTap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        
     @IBOutlet weak var imageView: UIImageView!
     @IBOutlet weak var textField: UITextField!
    
    var closeIcon = false
    var tagIsSelected = false
    
    let closeIconImage = UIImage(named: "closeIcon") as UIImage?
    let backIconImage = UIImage(named: "backArrow") as UIImage?
    let tagIconSelectedImage = UIImage(named: "tagSelectedIcon") as UIImage?

    
    @IBOutlet weak var tagButtonOutlet: UIButton!
    @IBOutlet weak var texttButtonOutlet: UIButton!
    @IBOutlet weak var locationButtonOutlet: UIButton!

     @IBOutlet weak var switchViewToCamera: UISegmentedControl!
    @IBOutlet weak var segmentedButtonOutlet: UISegmentedControl!
    
    
    //MARK: UPDATE...creare camera full screen customized
    @IBOutlet weak var previewView: UIView!
    var captureSession: AVCaptureSession?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    
 

    
    var imagePicker = UIImagePickerController()

     var galleryView = true
     var cameraView = true
    
    @IBOutlet weak var leftButtonOutlet: UIButton!
    @IBAction func backButton(_ sender: Any) {
        
        if closeIcon == false {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "customCameraVC")
            secondViewController.modalPresentationStyle = .fullScreen
            self.present(secondViewController, animated: true, completion: nil)
        }
        
        else  {
            closeIcon = false
            leftButtonOutlet.setImage(backIconImage, for: .normal)
            
        //MARK: ritorno allo stato iniziale --> UPDATE, torno a fotocamera custom
            //ho cliccato X devo tornare con segmented control e togliere icone a dx
            /*self.segmentedButtonOutlet.alpha = 1
            self.tagButtonOutlet.alpha = 0
            self.texttButtonOutlet.alpha = 0
            self.locationButtonOutlet.alpha = 0*/
       
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "customCameraVC")
            secondViewController.modalPresentationStyle = .fullScreen
            self.present(secondViewController, animated: true, completion: nil)
            
        }

    }
    
    @IBAction func tagButton(_ sender: Any) {
        //MARK: cambio icona con tagSelectedIcon e invoco funzioni DOTS
        self.tagButtonOutlet.setImage(tagIconSelectedImage, for: .normal)
        //MARK: quando è true si attiva touchesBegan in TapDetectorVC.swift
        tagIsSelected = true
        
        //view.addGestureRecognizer(doubleTap)
        view.addGestureRecognizer(recognizer)
    }
    @IBAction func textButton(_ sender: Any) {
        // MARK: Customized view to do in tag
        
        textField.alpha = 1
        textField.becomeFirstResponder()
        
        if textFieldShouldReturn(textField) {
            newPost.caption = textField.text!
            print(newPost.caption)
        }
    }
    @IBAction func locationButton(_ sender: Any) {
        //MARK: così non aggiungo altri pallini allo schermo
        tagIsSelected = false
        
        self.setDropDownStyleForLocation(dropDown: FCSDK.menu)
        
        
        
    }
    
    enum ImageSource {
        case photoLibrary
        case camera
    }

     @IBAction func indexChanged(_ sender: UISegmentedControl) {
         switch switchViewToCamera.selectedSegmentIndex {
         case 0:
             //galleryView.isHidden = true
             galleryView = false
             cameraView = true
            //MARK: apro fotocamera
            guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
                       selectImageFrom(.photoLibrary)
                       return
                   }
                   selectImageFrom(.camera)
         case 1:
             galleryView = true
             cameraView = false
            FCSDK.dotsArray.removeAll()
            self.imageView.reloadInputViews()
            
            //MARK: apro gallery
            //goToCamera()
            guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {
                       selectImageFrom(.camera)
                       return
                   }
                   selectImageFrom(.photoLibrary)
         default:
             break;
         }
     }
    
    
    
    func selectImageFrom(_ source: ImageSource){
           imagePicker =  UIImagePickerController()
           imagePicker.delegate = self
           switch source {
           case .camera:
               imagePicker.sourceType = .camera
           case .photoLibrary:
               imagePicker.sourceType = .photoLibrary
           }
           present(imagePicker, animated: true, completion: nil)
       }
     
    func goToCamera() {
         
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
         let secondViewController = storyboard.instantiateViewController(withIdentifier: "cameraPicker") // as! UIViewController
         secondViewController.modalPresentationStyle = .fullScreen
         self.present(secondViewController, animated: true, completion: nil)
     }
     
    
    var count = 0
    
    let arr = ["Gallery1.png", "Gallery2.png", "Gallery3.png", "Gallery4.png", "Gallery5.png", "Gallery6.png"]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        searchOutlet.alpha = 0
        profileOutlet.alpha = 0
        cameraOutlet.alpha = 0
        settingsOutlet.alpha = 0
        
        // immagine da visualizzare
        //self.imageView.contentMode = .scaleAspectFit
        //self.imageView.image = FCSDK.photoTaken

        
        
        //view.addGestureRecognizer(recognizer)
        recognizer.numberOfTapsRequired = 1
        
        //MARK: ICONE IN ALTO A DX inizialmente trasparenti
             self.tagButtonOutlet.alpha = 0
             self.texttButtonOutlet.alpha = 0
             self.locationButtonOutlet.alpha = 0
            
             //MARK: Text field trasparente e delegate (hide keyboard on return button)
             textField.alpha = 0
             textField.delegate = self
        
        //MARK: provo a muovere la label
        let gestureMoveLabel = UIPanGestureRecognizer(target: self, action: #selector(self.wasDragged(gestureRecognizer:)))
        textField.addGestureRecognizer(gestureMoveLabel)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        //self.imageView.image = FCSDK.photoTaken
        
        if FCSDK.pickFromGallery == true {
        //MARK: apro GALLERY
       /* guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {
                   selectImageFrom(.camera)
                   print("c'è problema qui")
                   return
               }*/
            
            UIImagePickerController.isSourceTypeAvailable(.photoLibrary)
            FCSDK.pickFromGallery = false //bug fixed: non devo aprire gallery dopo creazione dot
            selectImageFrom(.photoLibrary)
            
            
            self.imageView.contentMode = .scaleAspectFit
            //self.imageView.image = FCSDK.photoTaken
            
            
            
            
            //MARK: ICONE IN ALTO A DX inizialmente trasparenti
            self.tagButtonOutlet.alpha = 1
            self.texttButtonOutlet.alpha = 1
            self.locationButtonOutlet.alpha = 1
            self.segmentedButtonOutlet.alpha = 0
            

            
        }
        else if FCSDK.pickFromCamera == true { //se ha scattato la foto, mostro tag per mandare post
            
            self.imageView.contentMode = .scaleAspectFit
            self.imageView.image = FCSDK.photoTaken
            //MARK: ICONE IN ALTO A DX inizialmente trasparenti
            self.tagButtonOutlet.alpha = 1
            self.texttButtonOutlet.alpha = 1
            self.locationButtonOutlet.alpha = 1
            self.segmentedButtonOutlet.alpha = 0
            
            //MARK: la foto in JPEG per inviarla al SERVER
            if let imageData: Data = FCSDK.photoTaken!.pngData() {
                self.newPost.imageBase64String = imageData.base64EncodedString(options: .lineLength64Characters)
            }
            
            //Get data of existing UIImage
            let imageData = FCSDK.photoTaken!.jpegData(compressionQuality: 1)
            // Convert image Data to base64 encodded string
            if let imageBase64String = imageData?.base64EncodedString() {
                //print(imageBase64String ?? "Non ho potuto codificare l'immagine in Base64")
                self.newPost.imageBase64String = imageBase64String
            }
            
            //FCSDK.pickFromGallery = false
            FCSDK.pickFromCamera = false
        }
    }
    
    
    
    // function that helps drag the label around
   @objc func wasDragged(gestureRecognizer: UIPanGestureRecognizer) {

       let translation = gestureRecognizer.translation(in: self.view)
       let selectedLabel = gestureRecognizer.view!

      selectedLabel.center = CGPoint(x: selectedLabel.center.x,
                                      y: selectedLabel.center.y + translation.y)
       gestureRecognizer.setTranslation(CGPoint.zero, in: self.view)


       print(["1 x",self.textField.frame.origin.x,"y", self.textField.frame.origin.y])

   }
    
    
    @IBOutlet weak var fcButton: UIButton!
    @IBAction func FCButton(_ sender: Any) {
      count = count + 1
        
        
        if count.isEven {
            searchOutlet.alpha = 0
            profileOutlet.alpha = 0
            cameraOutlet.alpha = 0
            settingsOutlet.alpha = 0
        }
        else if count.isOdd {
            searchOutlet.alpha = 1
            profileOutlet.alpha = 1
            cameraOutlet.alpha = 1
            settingsOutlet.alpha = 1
            
            bubbleButton(button: searchOutlet)
            bubbleButton(button: profileOutlet)
            bubbleButton(button: cameraOutlet)
            bubbleButton(button: settingsOutlet)

        }

    }
    

    
    
    @IBOutlet weak var searchOutlet: UIButton!
    @IBOutlet weak var cameraOutlet: UIButton!
    @IBOutlet weak var settingsOutlet: UIButton!
    @IBOutlet weak var profileOutlet: UIButton!
    @IBAction func searchButton(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "search") // as! UIViewController
        secondViewController.modalPresentationStyle = .fullScreen
        self.present(secondViewController, animated: true, completion: nil)
    }
    
    
    // FOTO
    @IBAction func cameraButton(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "customCameraVC") // as! UIViewController
        secondViewController.modalPresentationStyle = .fullScreen
        self.present(secondViewController, animated: true, completion: nil)
    }
    
    // SOS BUTTON
    @IBAction func settingsButton(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "sos")
        secondViewController.modalPresentationStyle = .fullScreen
        self.present(secondViewController, animated: true, completion: nil)

        
    }
    @IBAction func profileButton(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
         let secondViewController = storyboard.instantiateViewController(withIdentifier: "profile") // as! UIViewController
         secondViewController.modalPresentationStyle = .fullScreen
         self.present(secondViewController, animated: true, completion: nil)
    }
    

    
    func bubbleButton(button: UIButton) {
        
        button.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)

        UIView.animate(withDuration: 1.0,
          delay: 0,
          usingSpringWithDamping: 0.2,
          initialSpringVelocity: 1.8,
          options: .allowUserInteraction,
          animations: { [weak button] in
            button?.transform = .identity
          },
          completion: nil)
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

           closeIcon = true  //per cambiare icona in alto a sinistra
           print("devo mettere icona X invece di back")
           leftButtonOutlet.setImage(closeIconImage, for: .normal)

           if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                   self.imageView.contentMode = .scaleAspectFit
                   self.imageView.image = pickedImage
            
            /*if let imageData: Data = pickedImage.pngData() {
                self.newPost.imageBase64String = imageData.base64EncodedString(options: .lineLength64Characters)
                //print(newPost.imageBase64String)
            }*/
            
            //Get data of existing UIImage
            let imageData = pickedImage.jpegData(compressionQuality: 1)
            // Convert image Data to base64 encodded string
            if let imageBase64String = imageData?.base64EncodedString() {
                //print(imageBase64String ?? "Non ho potuto codificare l'immagine in Base64")
                self.newPost.imageBase64String = imageBase64String
            }

                    
      
               }
            //MARK: ICONE IN ALTO A DX inizialmente trasparenti
            self.tagButtonOutlet.alpha = 1
            self.texttButtonOutlet.alpha = 1
            self.locationButtonOutlet.alpha = 1
            self.segmentedButtonOutlet.alpha = 0
        
           dismiss(animated: true, completion: nil)
       }
    
     
     // MARK: - Dismiss Keyboard Method
      func textFieldShouldReturn(_ textField: UITextField) -> Bool
      {
          textField.resignFirstResponder()
          return true
      }
     
     func setDropDownStyleForLocation(dropDown: DropDown) {

         // When drop down is displayed with `Direction.top`, it will be above the anchorView
         dropDown.topOffset = CGPoint(x: 0, y: (view.frame.size.width * view.frame.size.width)/2)
         
         dropDown.width = view.frame.size.width
         dropDown.cellHeight = 50

         
         /*** IMPORTANT PART FOR CUSTOM CELLS ***/
         //lo dichiaro in FCSK class
         
         DropDown.appearance().backgroundColor = UIColor.black
         DropDown.appearance().textColor = UIColor.white
         
         /*dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? PopupLocationCell else { return }

            // Setup your custom UI components
             cell.setupChooseDropDown()
             
         }*/
         /*** END - IMPORTANT PART FOR CUSTOM CELLS ***/

         dropDown.show()
         dropDown.dismissMode = .onTap
         dropDown.selectionAction = { index, title in
             print("index \(index) and \(title)")
             
             //MARK: invoco postSendNewPostCall con parametri correnti
             self.newPost.countryId = index  //countryId è int, passo al momento index riga
            
            self.newPost.caption = self.textField.text!
            print(self.newPost.caption)
            
            
            FCSDK.dotsDictionaryArray.removeAll()

             for element in FCSDK.dotsArray {
                 
                FCSDK.dotDictionary = element.getDictFormat()
                

                if let dizionario = FCSDK.dotDictionary  {
                    //FCSDK.dotsDictionaryArray?.append(dizionario)
                    FCSDK.dotsDictionaryArray.insert(dizionario, at: 0)
                }
                
                print(element.getDictFormat())
                
             }
            
            
            
            FCSDK.postSendPostCall(caption: self.newPost.caption, caption_pos_y_perc: Double(self.textField.frame.origin.y/1000), dots: FCSDK.dotsDictionaryArray, image: "data:image/jpeg;base64,\(self.newPost.imageBase64String)", success:{
                 print("ho invocato postSendPostCall")
                
             
                
                
                
                
             }, failure: { msg in
                 print("error")
             })
            
            //MARK: UPDATE invoco getPost per visualizzare la nuova foto caricata
            /*self.getPostAddLastCall(success:{
                print("invoco con successo getPost dopo aver inviato POST")
                FCSDK.filterIsSelected = false
                
                //Test nuovo metodo
                print("il post array contiene \(FCSDK.postArray.count) elementi")
                
                
            }, failure: { responseData in
                print(responseData)
            })*/
            
            //FCSDK.postArray.insert(self.newPost, at: 0)
            
            FCSDK.addNewPhotoToArray = true
            
            //torno a home
            let firstVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "goToFirstVC") as? FirstViewController
            firstVC?.modalPresentationStyle = .fullScreen
            self.present(firstVC!, animated: true, completion: nil)
            
         }
     }
    
    
    
    
}






extension GalleryViewController {
    
    
    func doubleTapDetected() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTappedSetDotButton))
              tap.numberOfTapsRequired = 1
              view.addGestureRecognizer(tap)
    }
    
    @objc func doubleTappedSetDotButton() {
         print("posso aggiungere dot, fatti due tocchi, poi diventa LONG TOUCH")
         //if FCSDK.filterIsSelected == false {
         let image = UIImage(named: "dot") as UIImage?
         let button   = UIButton(type: UIButton.ButtonType.custom) as UIButton
         button.frame = CGRect(x: self.dotX, y: self.dotY, width: 50, height: 50)
         button.setImage(image, for: .normal)
         
         self.view.addSubview(button)
         self.imageView.addSubview(button)

     }
    
    @objc func longPressHappened() {
        print("è un longpress")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let doppio: UITouch = (event?.allTouches?.first) {
            
            if let touch = touches.first,
               self.tagIsSelected == true && doppio.tapCount == 1 {
            print("ho toccato lo schermo: SINGLE TAP")
            let position = touch.location(in: self.view)
             print(position.x)
             print(position.y)
            
            self.dotX = (position.x)
            self.dotY = (position.y)
            
            print("creo il bottone DOT")
            let image = UIImage(named: "dot") as UIImage?
            let button   = UIButton(type: UIButton.ButtonType.custom) as UIButton
            button.frame = CGRect(x: self.dotX, y: self.dotY, width: 50, height: 50)
            button.setImage(image, for: .normal)
                    
            self.imageView.addSubview(button)
            
            //MARK: memorizzo il dot
            let newDot = Dot()
            newDot.posizioneX = Double(self.dotX)/1000
            newDot.posizioneY = Double(self.dotY)/1000
            newDot.description = "Descrizione Nuovo Articolo"
            newDot.brandId = 2
            newDot.colorId = 3
            newDot.itemId = 1
            newDot.gender = "f"
            
            //posizioni nel dot di Pivot
            FCSDK.newDotForGalleryPost = Dot()
            FCSDK.newDotForGalleryPost?.posizioneX = Double(self.dotX)/1000
            FCSDK.newDotForGalleryPost?.posizioneY = Double(self.dotY)/1000
            print(FCSDK.newDotForGalleryPost?.posizioneY as Any)
                
            self.newPost.dots?.append(newDot)
            
            //self.dots.append(newDot)
            //MARK: adesso passo il dot che comprende dati da PickerVC
            //if let dotDaAppendere = FCSDK.newDotForGalleryPost {
            //self.dots.append(FCSDK.newDotForGalleryPost ?? "non ci sono dot errore da Picker")
            //}

            print(self.dots.count)
            
            //MARK: creo form per inserire dettagli su articolo
            //let dropDown = DropDown()
            //setItemDotDetails(dropDown: dropDown)
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "pickerItemVC")
            secondViewController.modalPresentationStyle = .fullScreen
            self.present(secondViewController, animated: true, completion: nil)
            
         }
        }
        
    }
    
    func setItemDotDetails(dropDown: DropDown) {
        
        // When drop down is displayed with `Direction.top`, it will be above the anchorView
        dropDown.topOffset = CGPoint(x: 0, y: (view.frame.size.width * view.frame.size.width)/2)
        
        dropDown.width = view.frame.size.width
        dropDown.cellHeight = 200

        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = ["Item", "Color", "Brand", "Gender"]
        
        /*** IMPORTANT PART FOR CUSTOM CELLS ***/
        dropDown.cellNib = UINib(nibName: "PopupLocationCell", bundle: nil)
        

        
        DropDown.appearance().backgroundColor = UIColor.black
        DropDown.appearance().textColor = UIColor.white
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
           guard let cell = cell as? PopupLocationCell else { return }

           // Setup your custom UI components
            cell.setupChooseDropDown()
            
            dropDown.selectionAction = { index, title in
                print("Indice Dot \(index) e \(title)")
            }
            
        }
        /*** END - IMPORTANT PART FOR CUSTOM CELLS ***/

        dropDown.show()
        dropDown.dismissMode = .onTap
    }
    
    
}
