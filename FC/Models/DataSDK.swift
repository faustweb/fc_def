//
//  DataSDK.swift
//  FC
//
//  Created by Fausto Savatteri on 25/09/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import ObjectMapper

class DataSDK: Mappable {
    
    var brand: [Brand]?
    var item: [Item]?
    var circle : [Circle]?
    var color: [Color]?
    var id : String = ""
    
    
    required convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    
    func mapping(map: Map) {
        item                        <- map["item"]
        brand                    <- map["brand"]
        color                          <- map["color"]
        circle                          <- map["circle"]
        id                          <- map["id"]
    }

}
