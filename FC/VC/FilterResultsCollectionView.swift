//
//  FilterResultsCollectionView.swift
//  FC
//
//  Created by Fausto Savatteri on 30/11/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import UIKit
import FittedSheets
import BonMot
import Alamofire
import Kingfisher

//MARK: COLLECTION VIEW
extension ItemsTableViewFilter {
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        

        return filtroArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        

            let cellB = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell2", for: indexPath) as! ItemsCollectionViewCell  //CollectionViewCell è la mia custom class per definire la cella

            cellB.category.text = self.filtroArray[indexPath.item]
            //cellB.category.text = FCSDK.itemsArray[indexPath.item].itemName

            return cellB
    
    }
    

    
    func collectionView(_ collectionView: UICollectionView,
            didSelectItemAt indexPath: IndexPath) {
        
                //let cell = collectionView.cellForItem(at: indexPath)

            
        if (indexPath.item == 0) {
                    
                    print("Ho selezionato la prima cella: ITEM, sto già mostrando item su tableview")
                    if self.filtroIndex != 0 {
                        self.filtroIndex = 0
                        FCSDK.sezioneDaColorare = indexPath.item
                        self.tableView.reloadInputViews()
                        self.tableView.reloadData()
                    }
                }

                else if (indexPath.item == 1) {
                    print("Ho selezionato la seconda cella BRAND: mostro brand in table view")
                    self.filtroIndex = 1
                    FCSDK.sezioneDaColorare = indexPath.item
                    self.tableView.reloadInputViews()
                    self.tableView.reloadData()
                }
                else if (indexPath.item == 2) {
                    print("Ho selezionato la terza cella")
                    self.filtroIndex = 2
                    FCSDK.sezioneDaColorare = indexPath.item
                    self.tableView.reloadInputViews()
                    self.tableView.reloadData()
                }
                else if (indexPath.item == 3) {
                    print("Ho selezionato la quarta cella")
                    self.filtroIndex = 3
                    FCSDK.sezioneDaColorare = indexPath.item
                    self.tableView.reloadInputViews()
                    self.tableView.reloadData()
                }
        }
    
    // MARK: UICollectionViewDelegate
    
    
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        //var cell = collectionView.cellForItemAtIndexPath(indexPath)
        //cell.backgroundColor = UIColor.redColor()
       // cell.overlayView.backgroundColor = UIColor.clearColor()

    return true
    }
    
    // Uncomment this method to specify if the specified item should be selected
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
    return true
    }
    
    
    
    //MARK: metodi ausiliari
    func setCellStyleForLabelResults(_ cell: ItemsCollectionViewCell) {
        cell.resultsFilteredLabel.layer.cornerRadius = (cell.resultsFilteredLabel?.frame.size.height)!/4
        cell.resultsFilteredLabel?.layer.masksToBounds = true
        cell.resultsFilteredLabel?.layer.borderWidth = 1.3
        
    }
    
    func setLabelStyleForLabelResults(_ label: UILabel) {
        label.layer.cornerRadius = (label.frame.size.height)/4
        label.layer.masksToBounds = true
        label.layer.borderWidth = 1.3
        
    }
    
    func setViewStyleForLabelResults(_ view: UIView) {
        view.layer.cornerRadius = (view.frame.size.height)/4
        view.layer.masksToBounds = true
        view.layer.borderWidth = 1.3
        
    }
    
    
  
    
    func widthForView() -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: 32))
       label.numberOfLines = 0
       label.lineBreakMode = NSLineBreakMode.byWordWrapping
       //label.font = font
       //label.text = text

       label.sizeToFit()
       return label.frame.width
   }
    
    func updateLabelFrame(label: UILabel, size: CGSize) {
        let maxSize = CGSize(width: 150, height: 300)
        let size = label.sizeThatFits(maxSize)
        label.frame = CGRect(origin: CGPoint(x: 100, y: 100), size: size)
    }
    
    /*func setCellTitle(_ cell: ItemsCollectionViewCell, title: String) {
            cell.resultsFilteredLabel.text = title
            cell.resultsFilteredLabel.text = title.uppercased()
    }*/
    
    func setCellTitle(_ label: UILabel, title: String) {
            label.text = title
            label.text = title.uppercased()        
    }
    
}
