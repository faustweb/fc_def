//
//  ViewController.swift
//  Fashion Cratics
//
//  Created by Fausto Savatteri on 25/06/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import UIKit
import FittedSheets
import BonMot
import Alamofire
import ObjectMapper

class FirstViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var outletApplausometro: UIButton!
    @IBOutlet var menuView: UIView!
    @IBOutlet weak var searchOutlet: UIButton!
    @IBOutlet weak var cameraOutlet: UIButton!
    @IBOutlet weak var settingsOutlet: UIButton!
    @IBOutlet weak var profileOutlet: UIButton!
    @IBOutlet weak var filterIcon: UIButton!
    @IBOutlet weak var fcButton: UIButton!
    @IBOutlet var applausometroSliderBackground: UIView!
    
    var data : DataSDK? = nil
    var item : Item? = nil
    var color : Color? = nil
    
    var callSetVote = false
    
    var testPhotoArray = ["LaunchImage.png", "Camera1.png","LaunchImage.png", "Camera1.png"]
    
    var dotX: CGFloat = 0
    var dotY: CGFloat = 0
    
    let dotButton   = UIButton(type: UIButton.ButtonType.custom) as UIButton
    
    var indexPerDot = 0
    var indexSoloPath: IndexPath?
    
    var larghezzaSchermo: CGFloat?
    var altezzaSchermo: CGFloat?
    
    var showingDots = false
    
    override func viewDidAppear(_ animated: Bool) {
        if FCSDK.addNewPhotoToArray == true {
            self.getPostFirstCall(success:{
                FCSDK.addNewPhotoToArray = false
            }, failure: { responseData in
                print(responseData)
            })
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        FCSDK.parametersQueryFiltered = ""
        
        view.insetsLayoutMarginsFromSafeArea = false
        
        larghezzaSchermo = view.frame.size.width
        altezzaSchermo = view.frame.size.height
        
        menuView.isHidden = true

        
        if showingDots == true {
            filterIcon.setImage(UIImage(named: "FILTER_attivo.png"), for: .normal)
        } else if showingDots == false || FCSDK.filterLabelSelection == "ALL" {
            filterIcon.setImage(UIImage(named: "FILTER.png"), for: .normal)
        }
                
        slider.addTarget(self, action: #selector(sliderDidEndSliding), for: [.touchUpInside, .touchUpOutside])
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        hideApplausometro()
        setSliderStyle()
    }
    
    
    @IBOutlet weak var slider: UISlider!{
        didSet{
            slider.transform = CGAffineTransform(rotationAngle:(CGFloat.pi / -2))
            
            slider.setThumbImage(UIImage(named: "ApplausometroRotated.png"), for: UIControl.State.normal)
            slider.setThumbImage(UIImage(named: "ApplausometroRotated.png"), for: UIControl.State.highlighted)
            
            if self.slider.isContinuous == false {
                callSetVote = true
            }
        }
    }
    
    
    @objc func sliderDidEndSliding() -> Bool {
        postSetVote(postId: FCSDK.photoIdToVote, vote: (Int(slider.value)) )
        
        if let visibleRows = tableView.indexPathsForVisibleRows,
           visibleRows.count > 0 {
            
            let currentRow = visibleRows[0].row
            FCSDK.postArray[currentRow].votoCurrentUser = Int(slider.value)
        }
        
        return true
    }
    
    
    @IBAction func FCButton(_ sender: Any) {
        
        if !menuView.isHidden {
            menuView.isHidden = true
        } else {
            menuView.isHidden = false
            
            bubbleButton(button: searchOutlet)
            bubbleButton(button: profileOutlet)
            bubbleButton(button: cameraOutlet)
            bubbleButton(button: settingsOutlet)
        }
    }
    
    
    @IBAction func applausometro(_ sender: Any) {
        
        showApplausometro()
    
        slider.value = 0
        if let visibleRows = tableView.indexPathsForVisibleRows,
           visibleRows.count > 0 {
            
            let currentRow = visibleRows[0].row
            let vote = FCSDK.postArray[currentRow].votoCurrentUser
            slider.value = Float(vote)
        }
    }
    
    
    @IBAction func searchButton(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "search") // as! UIViewController
        secondViewController.modalPresentationStyle = .fullScreen
        self.present(secondViewController, animated: true, completion: nil)
        
    }
    
    
    // FOTO
    @IBAction func cameraButton(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "customCameraVC") // as! UIViewController
        secondViewController.modalPresentationStyle = .fullScreen
        self.present(secondViewController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
    }
    
    
    @IBAction func settingsButton(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "sos")
        secondViewController.modalPresentationStyle = .fullScreen
        self.present(secondViewController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func profileButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "profile") // as! UIViewController
        secondViewController.modalPresentationStyle = .fullScreen
        self.present(secondViewController, animated: true, completion: nil)
    }
    
    
    @IBAction func filterButton(_ sender: Any) {
        
        
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "createSingleSurveySheet")
        
        let contr = SheetViewController(controller: controller, sizes: [.fullScreen])
        self.present(contr, animated: true, completion: nil)
        
    }
    
    
    func bubbleButton(button: UIButton) {
        
        button.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        
        UIView.animate(withDuration: 1.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 1.8,
                       options: .allowUserInteraction,
                       animations: { [weak button] in
                        button?.transform = .identity
                       },
                       completion: nil)
    }
    
    
    func setSliderStyle() {
        applausometroSliderBackground.layer.cornerRadius = 12
        applausometroSliderBackground.clipsToBounds = true
    }
}


extension BinaryInteger {
    var isEven: Bool { return self % 2 == 0 }
    var isOdd: Bool { return self % 2 != 0 }
}


func presentationControllerShouldDismiss(_ presentationController: UIPresentationController) -> Bool {
    return false
}


func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    return true
}
