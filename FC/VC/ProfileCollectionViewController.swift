//
//  ProfileCollectionViewController.swift
//  FC
//
//  Created by Fausto Savatteri on 02/12/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import UIKit

private let reuseIdentifier = "profileCell"

extension ProfileVC: UICollectionViewDataSource, UICollectionViewDelegate {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return FCSDK.postArray.count
    }

    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "profileCell", for: indexPath) as! ProfileCollectionViewCell
        setPhoto(row: indexPath.item, photo: cell.profilePhoto)
        return cell
    }
    
    
    func setPhoto(row: Int, photo: UIImageView) {
        let imageUrl = FCSDK.postArray[row].urlImg
        let url = URL(string: imageUrl)
        photo.kf.setImage(with: url)
        photo.translatesAutoresizingMaskIntoConstraints = true
    }
}
