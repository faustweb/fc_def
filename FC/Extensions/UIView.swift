//
//  UIView.swift
//  FC
//
//  Created by Fausto Savatteri on 08/10/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

extension UIView
{
    func clearSubviews()
    {
        for subview in self.subviews as! [UIView] {
            subview.removeFromSuperview();
        }
    }
}


struct ContentView: View {
    
    @State private var countryIndex = 0
    
    var countries = ["Italy", "US", "Germany", "France"]
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    Picker(selection: $countryIndex, label: Text("Country")) {
                        ForEach(0 ..< countries.count) {
                            Text(self.countries[$0]).tag($0)
                        }
                    }
                }
            }
        }.navigationBarTitle(Text("Country"))
    }
    
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
