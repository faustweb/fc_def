//
//  PopupLocationCell.swift
//  FC
//
//  Created by Fausto Savatteri on 08/10/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import UIKit
import DropDown

class PopupLocationCell: DropDownCell {
    
    @IBOutlet weak var cityLabel: UILabel!

    @IBOutlet weak var leftButtonOutlet: UIButton!
    
    @IBOutlet weak var chooseButton: UIButton!

       @IBAction func choseButtonTapped(_ sender: Any) {
            
        print("ho selezionato chose button")
               //dismiss(animated: true, completion: nil)
            for element in FCSDK.countryArray {
                let city = element.countryName
                FCSDK.chooseDropDown.dataSource.append(city!)
            }
           
       }
       
       @IBOutlet weak var okayOutlet: UIButton!
       @IBAction func okButton(_ sender: Any) {

       }
    
    
    func setupChooseDropDown() {
        
        
        let chooseDropDown = DropDown()
        FCSDK.chooseDropDown.anchorView = chooseButton
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        FCSDK.chooseDropDown.bottomOffset = CGPoint(x: 0, y: optionLabel.bounds.height)
        
        /*for element in FCSDK.countryArray {
            let city = element.countryName
            FCSDK.chooseDropDown.dataSource.append(city!)
        }*/
        
        
        // Action triggered on selection
        FCSDK.chooseDropDown.selectionAction = { [weak self] (index, item) in
            self?.chooseButton.setTitle(item, for: .normal)
        }
    }
}
