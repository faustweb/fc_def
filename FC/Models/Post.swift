//
//  Post.swift
//  FC
//
//  Created by Fausto Savatteri on 29/09/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import ObjectMapper

class Post: Mappable {
    
    var id : Int = 0
    var userId: Int = 0
    var creatorUserId : Int?
    var creationTime : String = ""
    var nickname : String = ""
    var caption : String = ""
    var captionPosizioneAsseY : Double? = 0.5
    var urlImg : String = ""
    var urlThumbImg : String = ""
    var votoMedio : Int = 0
    var votoCurrentUser : Int = 0
    var circleId : String = ""
    var countryId : Int = 0
    var dots: [Dot]?
    var imageBase64String: String = ""
    
    
    required convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    
    func mapping(map: Map) {
        id                          <- map["id"]
        userId                      <- map["userid"]
        creatorUserId                        <- map["creatoruserid"]
        creationTime                    <- map["creationtime"]
        nickname                          <- map["nickname"]
        caption                          <- map["caption"]
        captionPosizioneAsseY                    <- map["caption_pos_y_perc"]
        urlImg                          <- map["image_url"]
        urlThumbImg                          <- map["image_thumb_url"]
        votoMedio                          <- map["vote_avg"]
        votoCurrentUser                          <- map["vote_currentuser"]
        circleId                          <- map["circleid"]
        countryId                          <- map["countryid"]
        dots                          <- map["dots"]
    }

}
