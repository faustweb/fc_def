//
//  PickerNewPostVC.swift
//  FC
//
//  Created by Fausto Savatteri on 15/10/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import UIKit
import FittedSheets
import BonMot
import Alamofire


class PickerNewPostVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    
    var gendersArray = ["FEMALE", "MALE"]
  
    
    var genderSelected = ""
    let newDot = Dot()

    
    @IBOutlet weak var pickerItemsView: UIPickerView!
    @IBOutlet weak var pickerColorsView: UIPickerView!
    @IBOutlet weak var pickerBrandsView: UIPickerView!
    @IBOutlet weak var pickerGenderView: UIPickerView!

    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        if pickerView == pickerItemsView  {
            
            return NSAttributedString(string: FCSDK.itemsArray[row].itemName!, attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            
        }
        
        else if pickerView == pickerBrandsView  {
            
            return NSAttributedString(string: FCSDK.brandsArray[row].brandName!, attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])

        }
        
        else if pickerView == pickerColorsView  {
            return NSAttributedString(string: FCSDK.colorArray[row].colorName!, attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            
        }
        return NSAttributedString(string: gendersArray[row], attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        if pickerView == pickerItemsView  || pickerView == pickerColorsView || pickerView == pickerBrandsView {
            return 1
        }
        
        return 1

    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerItemsView  {
            return FCSDK.itemsArray.count
        }
        
        else if pickerView == pickerBrandsView  {
            return FCSDK.brandsArray.count
        }
        
        else if pickerView == pickerColorsView  {
            return FCSDK.colorArray.count
        }
        
        //altrimenti gender
        return gendersArray.count
    }
    
 

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        if pickerView == pickerItemsView  {
            return FCSDK.itemsArray[row].itemName
            
        }
        
        else if pickerView == pickerBrandsView  {
            return FCSDK.brandsArray[row].brandName
        }
        
        else if pickerView == pickerColorsView  {
            return FCSDK.colorArray[row].colorName
        }
        return gendersArray[row]

    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerItemsView  {
            let itemId = FCSDK.itemsArray[row].itemId 
                newDot.itemId = itemId
                print(itemId)
                //print(FCSDK.itemsArray[row].itemId ?? "non esiste itemId/")
            
        }
        
        else if pickerView == pickerBrandsView  {
            let brandId = FCSDK.brandsArray[row].brandId
            newDot.brandId = brandId
            print(brandId)
            //if let brandId = FCSDK.brandsArray[row].brandId {
              //  newDot.brandId = brandId
            //}
        }
        
        else if pickerView == pickerColorsView  {
            if let colorId = FCSDK.colorArray[row].colorId {
                newDot.colorId = colorId
            }
        }
        else if pickerView == pickerGenderView  {
                genderSelected = gendersArray[row]
            
            if row == 0 {
                newDot.gender = "f"
            }
            else {
                newDot.gender = "m"
            }
        }
    }
    
    

    @IBAction func closeButton(_ sender: Any) {
        
        FCSDK.newDotForGalleryPost?.brandId = newDot.brandId
        FCSDK.newDotForGalleryPost?.itemId = newDot.itemId
        FCSDK.newDotForGalleryPost?.colorId = newDot.colorId
        FCSDK.newDotForGalleryPost?.gender = newDot.gender

        FCSDK.dotsArray.append(FCSDK.newDotForGalleryPost!)
        
        self.dismiss(animated: true, completion: nil)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        print("Sono dentro PickerVC")

        // Connect data:
        self.pickerItemsView.delegate = self
        self.pickerItemsView.dataSource = self
        self.pickerBrandsView.delegate = self
        self.pickerBrandsView.dataSource = self
        self.pickerColorsView.delegate = self
        self.pickerColorsView.dataSource = self
        self.pickerGenderView.delegate = self
        self.pickerGenderView.dataSource = self
        
        
        //nel caso in cui non selezioni nulla dal Picker
        self.newDot.brandId = 1
        self.newDot.colorId = 2
        self.newDot.gender = "f"
        self.newDot.itemId = 1
    }
    
    /*override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        <#code#>
    }*/
        
    
}
