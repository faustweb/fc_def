//
//  ItemsTableViewFilter.swift
//  Fashioncratic
//
//  Created by Fausto Savatteri on 26/06/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import UIKit
import FittedSheets
import BonMot
import Alamofire


class ItemsTableViewFilter: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var viewForLabelResults: UIView!
    
    @IBOutlet weak var searchController: UISearchBar!
    
    @IBOutlet weak var labelResults: UILabel!
    
    
    
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    
    @IBOutlet weak var clearButtonResults: UIButton!
    

    
    var filtroArray = ["ITEM","BRAND", "COUNTRY", "CIRCLE"]
    var filtroIndex = 0
    var isTagged = false

    
    @IBOutlet weak var collectionViewForFilteredLabels: UICollectionView!
    var cellForFilters = ItemsCollectionViewCell()

    @IBOutlet weak var collectionViewB: UICollectionView!
    
    
    var data : DataSDK? = nil
    var dataSlug = ""
    var item : Item? = nil
    var color : Color? = nil
    
    var post : Post? = nil
    
    var cellsInfoColorArray = [[String : Any]]()
    
    var cellIsAlreadySelected = false
    var selectedIndexPath: IndexPath? = nil
    
    var filterNumbers = 0
    
    var indice = IndexPath()
    

    
    @IBAction func clearButtonAction(_ sender: Any) {

        
        labelResults.text = "ALL"
        FCSDK.filterLabelSelection = "ALL"
        
        FCSDK.filterIsSelected = false
        
        self.tableView.deselectSelectedRow(animated: true)
        FCSDK.colorIdToSend = 0
        FCSDK.parametersQueryFiltered = ""
    }
    
    
    func enableAllColors() {
        blackButton.isEnabled = true
        greyButton.isEnabled = true
        purpleButton.isEnabled = true
        darkBlueButton.isEnabled = true
        lightBlueButton.isEnabled = true
        greenButton.isEnabled = true
        redButton.isEnabled = true
        orangeButton.isEnabled = true
        whiteButton.isEnabled = true
        FCSDK.coloriResetToTrue()
        
    }
    
    func disableAllColors() {
        blackButton.isEnabled = false
        greyButton.isEnabled = false
        purpleButton.isEnabled = false
        darkBlueButton.isEnabled = false
        lightBlueButton.isEnabled = false
        greenButton.isEnabled = false
        redButton.isEnabled = false
        orangeButton.isEnabled = false
        whiteButton.isEnabled = false
    }
    

    @IBOutlet weak var noColorButton: UIButton!
    @IBAction func noColorButtonAction(_ sender: Any) {
        enableAllColors()
        
        setCellTitle(labelResults, title: "ALL")
        //FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) NO COLOR"
        FCSDK.colorIdToSend = 0
    }
    

    
    
    @IBOutlet weak var blackButton: UIButton!
    @IBAction func blackButtonAction(_ sender: Any) {
        
        greyButton.isEnabled = false
        purpleButton.isEnabled = false
        darkBlueButton.isEnabled = false
        lightBlueButton.isEnabled = false
        greenButton.isEnabled = false
        redButton.isEnabled = false
        orangeButton.isEnabled = false
        whiteButton.isEnabled = false
        
        if labelResults.text == "ALL" {
            setCellTitle(labelResults, title: "BLACK")
            FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) BLACK"
            FCSDK.neroInserito = true
        }
        else if (FCSDK.neroInserito == true) {
            print("il colore è già inserito")
        }
        else { //concateno
            FCSDK.neroInserito = true
            labelResults.text = "\(labelResults.text!) - BLACK"
            FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) BLACK"
            
        }

        
        FCSDK.colorIdToSend = 1


    }
    @IBOutlet weak var greyButton: UIButton!
    @IBAction func greyButtonAction(_ sender: Any) {
        
        blackButton.isEnabled = false
        //greyButton.isEnabled = false
        purpleButton.isEnabled = false
        darkBlueButton.isEnabled = false
        lightBlueButton.isEnabled = false
        greenButton.isEnabled = false
        redButton.isEnabled = false
        orangeButton.isEnabled = false
        whiteButton.isEnabled = false
        
        if labelResults.text == "ALL" {
            setCellTitle(labelResults, title: "GREY")
            FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) GREY"
            FCSDK.grigioInserito = true
        }
        else if (FCSDK.grigioInserito == true) {
            print("il colore è già inserito")
        }
        else { //concateno
            FCSDK.grigioInserito = true
            labelResults.text = "\(labelResults.text!) - GREY"
            FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) GREY"
        }
        
       
        FCSDK.colorIdToSend = 2


    }
    @IBOutlet weak var purpleButton: UIButton!
    @IBAction func purpleButtonAction(_ sender: Any) {
        blackButton.isEnabled = false
        greyButton.isEnabled = false
        //purpleButton.isEnabled = false
        darkBlueButton.isEnabled = false
        lightBlueButton.isEnabled = false
        greenButton.isEnabled = false
        redButton.isEnabled = false
        orangeButton.isEnabled = false
        whiteButton.isEnabled = false
        

        
        if labelResults.text == "ALL" {
            setCellTitle(labelResults, title: "PURPLE")
            FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) PURPLE"
            FCSDK.violaInsrito = true
        }
        else if (FCSDK.violaInsrito == true) {
            print("il colore è già inserito")
        }
        else { //concateno
            FCSDK.violaInsrito = true
            labelResults.text = "\(labelResults.text!) - PURPLE"
            FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) PURPLE"
        }

        FCSDK.colorIdToSend = 3


    }
    @IBOutlet weak var darkBlueButton: UIButton!
    @IBAction func darkBlueButtonAction(_ sender: Any) {
        blackButton.isEnabled = false
        greyButton.isEnabled = false
        purpleButton.isEnabled = false
        //darkBlueButton.isEnabled = false
        lightBlueButton.isEnabled = false
        greenButton.isEnabled = false
        redButton.isEnabled = false
        orangeButton.isEnabled = false
        whiteButton.isEnabled = false
        
        
        
        if labelResults.text == "ALL" {
            setCellTitle(labelResults, title: "BLUE NAVY")
            FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) BLUE NAVY"
            FCSDK.bluInserito = true
        }
        else if FCSDK.bluInserito == true {
            print("il colore è già inserito")
        }
        else { //concateno
            labelResults.text = "\(labelResults.text!) - BLUE NAVY"
            FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) BLUE NAVY"
            FCSDK.bluInserito = true
        }
        
        FCSDK.colorIdToSend = 4


    }
    @IBOutlet weak var lightBlueButton: UIButton!
    @IBAction func lightBlueButtonAction(_ sender: Any) {
        blackButton.isEnabled = false
        greyButton.isEnabled = false
        purpleButton.isEnabled = false
        darkBlueButton.isEnabled = false
        //lightBlueButton.isEnabled = false
        greenButton.isEnabled = false
        redButton.isEnabled = false
        orangeButton.isEnabled = false
        whiteButton.isEnabled = false
        
        
        
        if labelResults.text == "ALL" {
            setCellTitle(labelResults, title: "LIGHT BLUE")
            FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) LIGHT BLUE"
            FCSDK.azzurroInserito = true
        }
        else if FCSDK.azzurroInserito == true {
            print("il colore è già inserito")
        }
        else { //concateno
            labelResults.text = "\(labelResults.text!) - LIGHT BLUEY"
            FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) LIGHT BLUE"
            FCSDK.azzurroInserito = true
        }
        
        FCSDK.colorIdToSend = 5


    }
    @IBOutlet weak var greenButton: UIButton!
    @IBAction func greenButtonAction(_ sender: Any) {
        blackButton.isEnabled = false
        greyButton.isEnabled = false
        purpleButton.isEnabled = false
        darkBlueButton.isEnabled = false
        lightBlueButton.isEnabled = false
        //greenButton.isEnabled = false
        redButton.isEnabled = false
        orangeButton.isEnabled = false
        whiteButton.isEnabled = false
        
   
        
        if labelResults.text == "ALL" {
            setCellTitle(labelResults, title: "GREEN")
            FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) GREEN"
            FCSDK.verdeInserito = true
        }
        else if FCSDK.verdeInserito == true {
            print("il colore è già inserito")
        }
        else { //concateno
            labelResults.text = "\(labelResults.text!) - GREEN"
            FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) GREEN"
            FCSDK.verdeInserito = true
        }

        FCSDK.colorIdToSend = 6

    }
    @IBOutlet weak var redButton: UIButton!
    @IBAction func redButtonAction(_ sender: Any) {
        blackButton.isEnabled = false
        greyButton.isEnabled = false
        purpleButton.isEnabled = false
        darkBlueButton.isEnabled = false
        lightBlueButton.isEnabled = false
        greenButton.isEnabled = false
        //redButton.isEnabled = false
        orangeButton.isEnabled = false
        whiteButton.isEnabled = false
        
        if labelResults.text == "ALL" {
            setCellTitle(labelResults, title: "RED")
            FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) RED"
            FCSDK.rossoInsrito = true
        }
        else if FCSDK.rossoInsrito == true {
            print("il colore è già inserito")
        }
        else { //concateno
            labelResults.text = "\(labelResults.text!) - RED"
            FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) RED"
            FCSDK.rossoInsrito = true
        }
        
        FCSDK.colorIdToSend = 7


    }
    @IBOutlet weak var orangeButton: UIButton!
    @IBAction func orangeButtonAction(_ sender: Any) {
        blackButton.isEnabled = false
        greyButton.isEnabled = false
        purpleButton.isEnabled = false
        darkBlueButton.isEnabled = false
        lightBlueButton.isEnabled = false
        greenButton.isEnabled = false
        redButton.isEnabled = false
        //orangeButton.isEnabled = false
        whiteButton.isEnabled = false
        
        if labelResults.text == "ALL" {
            setCellTitle(labelResults, title: "ORANGE")
            FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) ORANGE"
            FCSDK.arancioneInsrito = true
        }
        else if FCSDK.arancioneInsrito == true {
            print("il colore è già inserito")
        }
        else { //concateno
            labelResults.text = "\(labelResults.text!) - ORANGE"
            FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) ORANGE"
            FCSDK.arancioneInsrito = true
        }
        
        FCSDK.colorIdToSend = 8


    }
    @IBOutlet weak var whiteButton: UIButton!
    @IBAction func whiteButtonAction(_ sender: Any) {
        blackButton.isEnabled = false
        greyButton.isEnabled = false
        purpleButton.isEnabled = false
        darkBlueButton.isEnabled = false
        lightBlueButton.isEnabled = false
        greenButton.isEnabled = false
        redButton.isEnabled = false
        orangeButton.isEnabled = false
        //whiteButton.isEnabled = false
        
        
        if labelResults.text == "ALL" {
            setCellTitle(labelResults, title: "BIANCO")
            FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) BIANCO"
            FCSDK.biancoInserito = true
        }
        else if FCSDK.biancoInserito == true {
            print("colore bianco già inserito")
        }
        else { //concateno
            labelResults.text = "\(labelResults.text!) - BIANCO"
            FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) BIANCO"
            FCSDK.biancoInserito = true
        }
        
        
        FCSDK.colorIdToSend = 9


    }
    

    

    
    @IBOutlet weak var FCButton: UIButton!
    @IBAction func FCButtonAction(_ sender: Any) {
                
        if self.labelResults.text == "ALL" {
            FCSDK.filterNumbers = 0

            FCSDK.stringToSendForFilter = "manage-posts/posts"
            FCSDK.filterIsSelected = false
        }
        else if FCSDK.colorIdToSend == 0 {

            FCSDK.stringToSendForFilter = "manage-posts/posts?\(FCSDK.parametersQueryFiltered)"
            FCSDK.filterIsSelected = true
        }
        
        else if FCSDK.parametersQueryFiltered == "" {
            FCSDK.stringToSendForFilter = "manage-posts/posts?colorid=\(FCSDK.colorIdToSend)"
            FCSDK.filterIsSelected = true
        }
        else {
            FCSDK.stringToSendForFilter = "manage-posts/posts?\(FCSDK.parametersQueryFiltered)&colorid=\(FCSDK.colorIdToSend)"
            FCSDK.filterIsSelected = true
        }
        
        getPostFilteredCall(success:{
            print("invoco con successo getPost")
            //Test nuovo metodo
            print("il post array contiene \(FCSDK.postArray.count) elementi")
                
                //visualizzo ITEMS con filtro applicato
            let firstVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "goToFirstVC") as? FirstViewController
            firstVC?.modalPresentationStyle = .fullScreen
            self.present(firstVC!, animated: true, completion: nil)
            
            
        }, failure: { responseData in
            print(responseData)
        })
    }
    




    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //setLabelsStyle()
        
        
        if FCSDK.filterIsSelected == false {
            labelResults.text = "ALL"
            FCSDK.filterLabelSelection = "ALL"
        }
        
        setViewStyleForLabelResults(self.viewForLabelResults)
        
       /* else if FCSDK.filterIsSelected == true && FCSDK.filterLabelSelection != "ALL"{
        //è true, filtro attivo, visualizza le ricerche salvate in precedenza
            
            if FCSDK.filterNumbers == 1 {
                labelResults.text = FCSDK.filterLabelSelection
            }
            else if FCSDK.filterNumbers == 2 {
                label2.alpha = 1
                labelResults.text = FCSDK.filterLabelSelection
                label2.text = FCSDK.filterLabel2
            }
            else if FCSDK.filterNumbers == 3 {
                label2.alpha = 1
                label3.alpha = 1
                labelResults.text = FCSDK.filterLabelSelection
                label2.text = FCSDK.filterLabel2
                label3.text = FCSDK.filterLabel3

            }
            else if FCSDK.filterNumbers == 4 {
                label2.alpha = 1
                label3.alpha = 1
                label4.alpha = 1
                labelResults.text = FCSDK.filterLabelSelection
                label2.text = FCSDK.filterLabel2
                label3.text = FCSDK.filterLabel3
                label4.text = FCSDK.filterLabel4

            }*/
            
          
        //}
        

        labelResults.text = FCSDK.filterLabelSelection

        
        print("il circleArray quanti elementi ha?")
        print(FCSDK.circleArray.count)
        
        tableView.delegate = self
        tableView.dataSource = self
                
        collectionViewB.delegate = self
        collectionViewB.dataSource = self

        self.view.addSubview(collectionViewB)

        print("la dimensione di colorArray usato come test è:")
        print(FCSDK.colorArray.count)
        
  
    

    }
    
    override func viewWillAppear(_ animated: Bool) {

            super.viewWillAppear(animated)
            self.tableView.reloadData()
        
        if FCSDK.filterIsSelected == true && FCSDK.filterLabelSelection != "ALL"{ //è true, significa che voglio filtrare la seconda volta, faccio vedere cosa avevo cercato prima
            
            //labelResults.text = FCSDK.filterLabelSelection
            
            self.tableView.selectRow(at: FCSDK.rigaDaColorare, animated: true, scrollPosition: .middle)

        }
            
    
      }
    
    
}
 

extension ItemsTableViewFilter: UITableViewDelegate {
    


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let cell = tableView.cellForRow(at: indexPath) as! ItemsTableViewCell
        self.cellIsAlreadySelected = true
        indice = indexPath
        
   
        
        if filtroIndex == 0 {
            //ITEMS
            setResults(indice: indice, cell: cell)
            FCSDK.filterIsSelected = true
            //FCSDK.itemIdToSend = FCSDK.itemsArray[indexPath.row].itemId
            setQueryForServer(string: "itemid=\(FCSDK.itemsArray[indexPath.row].itemId)")
            isTagged = true
            

    
        }
        else if filtroIndex ==  1 { //BRAND
            
       
            
            setResults(indice: indice, cell: cell)

            FCSDK.filterIsSelected = true
            //FCSDK.brandIdToSend = FCSDK.brandsArray[indexPath.row].brandId ?? 0
            setQueryForServer(string: "brandid=\(FCSDK.brandsArray[indexPath.row].brandId)")

            
            isTagged = true
            

        }
        else if filtroIndex ==  2 { //COUNTRY
            setResults(indice: indice, cell: cell)
            FCSDK.filterIsSelected = true
            //FCSDK.countryIdToSend = FCSDK.countryArray[indexPath.row].countryId
            setQueryForServer(string: "countryid=\(FCSDK.countryArray[indexPath.row].countryId)")

            
            isTagged = true



        }
        else if filtroIndex ==  3 { //CIRCLE
            setResults(indice: indice, cell: cell)
            FCSDK.filterIsSelected = true
            //FCSDK.circleIdToSend = FCSDK.circleArray[indexPath.row].circleId
            setQueryForServer(string: "circleid=\(FCSDK.circleArray[indexPath.row].circleId)")

            isTagged = true
            

        }
    }
            
        //MARK: UPDATE, lo invoco con pressione FC
        //chiama post filtrato con immagine di esempio e poi chiama post set vote
            

}


//MARK: TABLE VIEW
extension ItemsTableViewFilter: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if filtroIndex == 0 {
                //ITEMS
                return FCSDK.itemsArray.count
            }
            else if filtroIndex ==  1 { //BRAND
                return FCSDK.brandsArray.count

            }
            else if filtroIndex ==  2 { //COUNTRY
                return FCSDK.countryArray.count
            }
            else if filtroIndex ==  3 { //CIRCLE
                return FCSDK.circleArray.count
            }
            return FCSDK.itemsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ItemsTableViewCell
        
        let backgroundView = UIView()
        let customColor = UIColor(hexString: "#d4af37")  //uso mia extension per esprimere colore in hex
        backgroundView.backgroundColor = customColor
        cell.selectedBackgroundView = backgroundView
        
        
        
        if filtroIndex == 0 {
            //ITEMS
            cell.item.text = FCSDK.itemsArray[indexPath.row].itemName
            cell.item.text = cell.item.text?.uppercased()
            
        }
        else if filtroIndex ==  1 { //BRAND
            cell.item.text = FCSDK.brandsArray[indexPath.row].brandName
            cell.item.text = cell.item.text?.uppercased()

        }
        else if filtroIndex ==  2 { //COUNTRY
            cell.item.text = FCSDK.countryArray[indexPath.row].countryName
            cell.item.text = cell.item.text?.uppercased()

        }
        else if filtroIndex ==  3 { //CIRCLE
            cell.item.text = FCSDK.circleArray[indexPath.row].circleName
            cell.item.text = cell.item.text?.uppercased()

        }

        
        
        return cell
    }
    
    
    
    //MARK: Metodi ausiliari
    func setFilterToAll() {
        FCSDK.filterLabelSelection = "ALL"
        FCSDK.itemIdToSend = 0
        FCSDK.brandIdToSend = 0
        FCSDK.countryIdToSend = 0
        FCSDK.circleIdToSend = 0
        FCSDK.parametersQueryFiltered = ""
                
        FCSDK.filterIsSelected = false //filtro disattivo se deseleziono
        isTagged = false
        
        setCellTitle(labelResults, title: "ALL")
        
    }
    
    func setResults(indice: IndexPath, cell: ItemsTableViewCell) {
        /*if selectedIndexPath == indice && FCSDK.filterIsSelected == true {
            // it was already selected
            print(cell.item.text!)
            print("era già selezionato, quindi rimuovo")
            selectedIndexPath = nil
            tableView.deselectRow(at: indice, animated: false)
            setFilterToAll()
        }*/
        
       // wasn't yet selected, so let's remember it
            print(cell.item.text!)
            FCSDK.rigaDaColorare = indice
            selectedIndexPath = indice
            
            if labelResults.text != "ALL" {
                labelResults.text = "\(labelResults.text!) - \(cell.item.text!)"
            }
            else {
                labelResults.text = cell.item.text!
            }
    }
    
    func setQueryForServer(string: String) {
        if FCSDK.parametersQueryFiltered == "" {
            FCSDK.parametersQueryFiltered = string
        }
        else {
            FCSDK.parametersQueryFiltered = "\(FCSDK.parametersQueryFiltered)&\(string)"
            print(FCSDK.parametersQueryFiltered)
        }
    }

}




extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}

extension UITableView {

    func deselectSelectedRow(animated: Bool)
    {
        if let indexPathForSelectedRow = self.indexPathForSelectedRow {
            self.deselectRow(at: indexPathForSelectedRow, animated: animated)
        }
    }

}
