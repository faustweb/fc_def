//
//  Circle.swift
//  FC
//
//  Created by Fausto Savatteri on 25/09/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import ObjectMapper

class Circle: Mappable {
    
    var circleId: Int = 0
    var circleName: String? = ""
    var imageUrl: String? = ""
    
    
    required convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    
    func mapping(map: Map) {
        circleId                        <- map["id"]
        circleName                    <- map["name"]
        imageUrl                    <- map["image_url"]
    }

}
