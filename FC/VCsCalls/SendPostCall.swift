//
//  SendPostCall.swift
//  FC
//
//  Created by Fausto Savatteri on 12/10/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import UIKit
import SwiftKeychainWrapper
import SwiftUI

protocol SendPostCall {
    func postSendPostCall()
}

extension CameraViewController {
    
    func postSendPostCall(caption: String, caption_pos_y_perc: Double, dots: [[String:Any]], image: String, success:@escaping ()->(), failure:@escaping (String)->()) {
        postSendPost(caption: caption, caption_pos_y_perc: caption_pos_y_perc, dots: dots, image: image, success: { responseData in
            print("success")
                            }, failure: { responseData in
                                failure(FCSDK.handleAlamofireError(responseData: responseData))
                            })
      }
    
    func postSendPost(caption: String, caption_pos_y_perc: Double, dots: [[String:Any]], image: String, success:@escaping (Data)->(), failure:@escaping (Any?)->()) {
        

       
        
        
        let parameters: Parameters = ["caption": caption, "caption_pos_y_perc": caption_pos_y_perc, "dots": dots, "image": image ]

         
        
        AF.request(FCSDK.rootUrl + "manage-posts/posts", method: .post, parameters: parameters, encoding: JSONEncoding.default).validate(statusCode: 200..<300).responseData { response in

            
            
                    switch response.result {
                    case .success:
                        if response.error == nil {
                            success(response.data!)
                            debugPrint("Response: \(response)")
                            
                        } else {
                            failure(response.data!)
                        }
                    case .failure(let error):
                        failure(error)
                    }
            }
        //} //end for ObjectMapper
    }
    
    
   
    
}

