//
//  CameraViewController.swift
//  FC
//
//  Created by Fausto Savatteri on 02/07/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import UIKit
import FittedSheets
import BonMot
import Alamofire
import DropDown
import SwiftUI

class CameraViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate {
    
    let newPost = Post()
    var dots = [Dot]()
    
    @IBOutlet weak var switchViewToCamera: UISegmentedControl!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var textField: UITextField!

    
    var imagePicker = UIImagePickerController()

    var dotX: CGFloat = 0
    var dotY: CGFloat = 0
    
    var galleryView = true
    var cameraView = true
    
    var closeIcon = false
    var tagIsSelected = false
    
    let closeIconImage = UIImage(named: "closeIcon") as UIImage?
    let backIconImage = UIImage(named: "backArrow") as UIImage?
    let tagIconSelectedImage = UIImage(named: "tagSelectedIcon") as UIImage?

    
    @IBOutlet weak var tagButtonOutlet: UIButton!
    @IBOutlet weak var texttButtonOutlet: UIButton!
    @IBOutlet weak var locationButtonOutlet: UIButton!

    @IBOutlet weak var segmentedButtonOutlet: UISegmentedControl!
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        switch switchViewToCamera.selectedSegmentIndex {
        case 0:
            galleryView = false
            cameraView = true
        case 1:
            galleryView = true
            cameraView = false
            goToGallery()
        default:
            break;
        }
    }
    
    @IBOutlet weak var leftButtonOutlet: UIButton!
    @IBAction func backButton(_ sender: Any) {
        
        if closeIcon == false {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "goToFirstVC")
            secondViewController.modalPresentationStyle = .fullScreen
            self.present(secondViewController, animated: true, completion: nil)
        }
        
        else  {
            closeIcon = false
            leftButtonOutlet.setImage(backIconImage, for: .normal)
            
            //MARK: ritorno allo stato iniziale
            //ho cliccato X devo tornare con segmented control e togliere icone a dx
            self.segmentedButtonOutlet.alpha = 1
            self.tagButtonOutlet.alpha = 0
            self.texttButtonOutlet.alpha = 0
            self.locationButtonOutlet.alpha = 0
        }

    }
    
    @IBAction func tagButton(_ sender: Any) {
        //MARK: cambio icona con tagSelectedIcon e invoco funzioni DOTS
        self.tagButtonOutlet.setImage(tagIconSelectedImage, for: .normal)
        //MARK: quando è true si attiva //Began in TapDetectorVC.swift
        tagIsSelected = true
    }
    @IBAction func textButton(_ sender: Any) {
        // MARK: Customized view to do in tag
        
        textField.alpha = 1
        textField.becomeFirstResponder()
        
        if textFieldShouldReturn(textField) {
            newPost.caption = textField.text!
            print(newPost.caption)
        }
    }
    @IBAction func locationButton(_ sender: Any) {
        //let dropDown = DropDown()
        setDropDownStyleForLocation(dropDown: FCSDK.menu)
        

        
    }
    
    func goToGallery() {
         
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
         let secondViewController = storyboard.instantiateViewController(withIdentifier: "cameraRoll") // as! UIViewController
        secondViewController.modalPresentationStyle = .fullScreen
         self.present(secondViewController, animated: true, completion: nil)
     }
    
    
    
    var count = 0
    
    let arr = ["Gallery1.png", "Gallery2.png", "Gallery3.png", "Gallery4.png", "Gallery5.png", "Gallery6.png"]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        
        //MARK: ICONE IN ALTO A DX inizialmente trasparenti
        self.tagButtonOutlet.alpha = 0
        self.texttButtonOutlet.alpha = 0
        self.locationButtonOutlet.alpha = 0
       
        //MARK: Text field trasparente e delegate (hide keyboard on return button)
        textField.alpha = 0
        textField.delegate = self
        
        
        //MARK: IMAGE PICKER
        imagePicker.delegate = self
        guard !UIImagePickerController.isSourceTypeAvailable(.camera) else {
            
            return
            
        }

        let picker = UIImagePickerController()
          //picker.delegate = self
          picker.allowsEditing = true

          if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.sourceType = .camera
            picker.modalPresentationStyle = .fullScreen
          } else {
            print("non trovo la fotocamera")
            //picker.sourceType = .photoLibrary
            //picker.modalPresentationStyle = .fullScreen
          }
        present(imagePicker, animated: true, completion: nil)
        

    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

           closeIcon = true  //per cambiare icona in alto a sinistra
           print("devo mettere icona X invece di back")
           leftButtonOutlet.setImage(closeIconImage, for: .normal)

           if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                   self.imageView.contentMode = .scaleAspectFit
                   self.imageView.image = pickedImage
            
            if let imageData: Data = pickedImage.pngData() {
                self.newPost.imageBase64String = imageData.base64EncodedString(options: .lineLength64Characters)
            }
            
            //Get data of existing UIImage
            let imageData = pickedImage.jpegData(compressionQuality: 1)
            // Convert image Data to base64 encodded string
            if let imageBase64String = imageData?.base64EncodedString() {
                //print(imageBase64String ?? "Non ho potuto codificare l'immagine in Base64")
                self.newPost.imageBase64String = imageBase64String
            }
      
               }
            //MARK: ICONE IN ALTO A DX inizialmente trasparenti
            self.tagButtonOutlet.alpha = 1
            self.texttButtonOutlet.alpha = 1
            self.locationButtonOutlet.alpha = 1
            self.segmentedButtonOutlet.alpha = 0
        
           dismiss(animated: true, completion: nil)
       }
    
    // MARK: - Dismiss Keyboard Method
     func textFieldShouldReturn(_ textField: UITextField) -> Bool
     {
         textField.resignFirstResponder()
         return true
     }
    
    func setDropDownStyleForLocation(dropDown: DropDown) {

        // When drop down is displayed with `Direction.top`, it will be above the anchorView
        dropDown.topOffset = CGPoint(x: 0, y: (view.frame.size.width * view.frame.size.width)/2)
        
        dropDown.width = view.frame.size.width
        dropDown.cellHeight = 50

        
        /*** IMPORTANT PART FOR CUSTOM CELLS ***/
        //lo dichiaro in FCSK class
        
        DropDown.appearance().backgroundColor = UIColor.black
        DropDown.appearance().textColor = UIColor.white
        
        /*dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
           guard let cell = cell as? PopupLocationCell else { return }

           // Setup your custom UI components
            //cell.optionLabel.text = FCSDK.circleArray[index].circleName
            cell.setupChooseDropDown()
            
        }*/
        /*** END - IMPORTANT PART FOR CUSTOM CELLS ***/

        dropDown.show()
        dropDown.dismissMode = .onTap
        dropDown.selectionAction = { index, title in
            print("index \(index) and \(title)")
            
            //MARK: invoco postSendNewPostCall con parametri correnti
            self.newPost.countryId = index  //countryId è int, passo al momento index riga
            //newPost.caption già memorizzato
            /*for element in self.dots {
                
               FCSDK.dotDictionary = element.getDictFormat()
               FCSDK.dotsDictionaryArray?.append(FCSDK.dotDictionary!)
               
               print(element.getDictFormat())
               
            }
            
            self.postSendPostCall(caption: self.newPost.caption, caption_pos_y_perc: 0.51, dots: FCSDK.dotsDictionaryArray!, image: self.newPost.imageBase64String, success:{
                print("ho invocato postSendPostCall")
            }, failure: { msg in
                print("error")
            })*/
        }
    }
    
}



